<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
    <!-- css -->
    <link rel="stylesheet" href="../css/Index.css">
    <!-- bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm"
        crossorigin="anonymous"></script>
    <!-- icon -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.2/font/bootstrap-icons.css">
      <!-- jQuery -->
  <script src="../js/jquery.js"></script>
    <style>
        body {
            background-color: #f4f4f4;
        }

        .login-container {
            max-width: 400px;
            margin: 0 auto;
            background-color: #fff;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.2);
        }
    </style>
</head>

<body>
    <div class="headerTop">
        <p class="headerTopText">Mon - Sat | 09:00AM - 10:00PM</p>
        <p class="headerTopText">+91 9874563210</p>
    </div>
    <nav class="navbar sticky-top navbar-expand-lg headerNav">
        <div class="container-fluid">
            <a class="navbar-brand" href="./index.php" style="color: #00A3C8;">
                <h2>MedHelp</h2>
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0 ms-5">
                    <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="./index.php">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="./Services.php">Services</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="./About.php">About Us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="./Contact.php">Contact Us</a>
                    </li>
                </ul>
            </div>
        </div>
        </div>
    </nav>
    <div class="container my-5">
        <div class="login-container rounded-4 py-5 px-4" style="max-width: 550px">
            <h2 style="color: #004861;" class="text-center pt-2">Sign Up for <span
                    style="color: #00A3C8;">MedHelp</span></h2>
            <p class="text-center">Connect with the best local pros</p>
            <div>
                <div class="form-group my-4">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text py-2">
                                <i class="bi bi-person"></i>
                            </span>
                        </div>
                        <input type="text" class="form-control py-2" id="name" placeholder="Enter Name" required>
                    </div>
                </div>
                <div class="form-group my-4">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text py-2">
                                <i class="bi bi-envelope-at"></i>
                            </span>
                        </div>
                        <input type="email" class="form-control py-2" id="email" placeholder="Enter E-mail" required>
                    </div>
                </div>
                <div class="form-group my-4">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="bi bi-lock"></i>
                            </span>
                        </div>
                        <input type="password" class="form-control" id="pwd" placeholder="Enter Password" required>
                    </div>
                </div>
                <div class="form-group my-4">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="bi bi-lock"></i>
                            </span>
                        </div>
                        <input type="password" class="form-control" id="cpwd" placeholder="Enter Confirm Password"
                            required>
                    </div>
                </div>
                <div id='showErr' class="alert alert-danger ms-1 me-1" style="display: none;" role="alert">
                </div>
                <button type="submit" id="btnRegister" class="btn btn-primary btn-block w-100">Register</button>
                <p class="text-center pt-3">Already have an account? <a href="./LoginPage.php"
                        class="text-decoration-none">Sign In</a></p>
                </div>
        </div>
    </div>

    <!-- footer -->
    <footer class="" style="background-color: #004861;">
        <div class="footer-container d-grid text-white-50 container py-4">
            <div class="py-3 d-flex flex-column">
                <h1>MedHelp</h1>
                <p class="pt-4" style="padding-right: 15%;">Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Distinctio aperiam ad quisquam voluptate aut ab?</p>
            </div>
            <div class="py-3 d-flex flex-column">
                <h4>Quick Links</h4>
                <a href="./index.html" class="text-white-50 py-1 w-50">Home</a>
                <a href="./Services.php" class="text-white-50 py-1 w-50">Services</a>
                <a href="./About.php" class="text-white-50 py-1 w-50">About us</a>
                <a href="./Contact.php" class="text-white-50 py-1 w-50">Contact us</a>
            </div>
            <div class="py-3">
                <h4>Contact us</h4>
                <p><i class="bi-pin-fill"></i> 401-Shreeram complex, Ahmedabad - 392324</p>
                <p><i class="bi-telephone"></i> +91 9876543210</p>
                <p><i class="bi-envelope"></i> medhelp@gmail.com</p>
            </div>
            <div class="py-3">
                <a href="#" class="fa fa-instagram mx-2 bg-gradient p-2 text-white"></a>
                <a href="#" class="fa fa-facebook-official mx-2 bg-gradient p-2 text-white"></a>
                <a href="#" class="fa fa-linkedin mx-2 bg-gradient p-2 text-white"></a>
                <p><sup>©</sup>Do not sell or share My Professional information.</p>
            </div>
        </div>
    </footer>

    <script>
        $("#btnRegister").click(function(){
            let name = $("#name").val();
            let email = $("#email").val();
            let pass = $("#pwd").val();
            let cpass = $("#cpwd").val();
            let img = "../img/user.jpeg"; 
            let name_pattern = /[a-z A-Z]/i;
            let email_pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        

            if(name.match(name_pattern) && name != "" && email.match(email_pattern) && pass != "" && cpass == pass && img != ""){
                $.ajax({
                        url: '../componants/insert.php',
                        method: 'post',
                        data: { 'table': "register", "name" : name, "email" : email, "pass" : pass, "type": "user", "picture": img},
                        success: function (result) {
                            if(result == "Inserted"){
                                location.replace("./LoginPage.php");
                            }
                        }
                })
            }
            else{
                $("#showErr").text("Please enter valid details for register account");
                $("#showErr").css("display", "block")
            }
        })
    </script>
</body>

</html>