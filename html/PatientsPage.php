<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Patients</title>

    <link rel="stylesheet" href="../css/navbar.css">
    <link rel="stylesheet" href="../css/Index.css">

    <!-- bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm"
        crossorigin="anonymous"></script>

    <!-- icons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.2/font/bootstrap-icons.css">
    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.js"></script>
    
    <!-- jQuery -->
    <script src="../js/jquery.js"></script>

    <style>
        tr {
            text-align: center;
        }
    </style>
</head>
<?php
        include "../componants/connection.php";

        session_start();
        if(isset($_SESSION["userId"])){
            $Id =  $_SESSION["userId"];
        }
        else{
            $Id = "";
            $userName = "";
            $userImg = "";
            $userpass = "";
            $userType = "";
            $userEmail = "";
        }

        if($Id){
            $sql = "SELECT * FROM `tblRegister` WHERE `id`=$Id";
            $result = mysqli_query($conn, $sql) or die("Unscuccessfull");
            if(mySqli_num_rows($result) == 1){
               while($row = mysqli_fetch_assoc($result)){
                  $userName = $row['name'];
                  $userImg = $row['picture'];
                  $userpass = $row['password'];
                  $userType = $row['type'];
                  $userEmail = $row['email'];
               }
            }
        }
       
        
    
    ?>
<body>
    <!-- header -->
    <div class="headerTop">
        <p class="headerTopText">Mon - Sat | 09:00AM - 10:00PM</p>
        <p class="headerTopText">+91 9874563210</p>
    </div>
<nav class="navbar sticky-top navbar-expand-lg headerNav">
    <div class="container-fluid">
      <a class="navbar-brand" href="./index.php" style="color: #00A3C8;">
        <h2>MedHelp</h2>
      </a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0 ms-5">
          <li class="nav-item">
            <a class="nav-link " aria-current="page" href="./index.php">Home</a>
          </li>
          <li class="nav-item" id="deshborad">
            <a class="nav-link" href="./AdminDeshboradPage.php">Dashboard</a>
          </li>
          <li class="nav-item" id="appoinment">
            <button class="nav-link" onclick="appoinmentLink()">Appoinment</button>
          </li>
          <li class="nav-item" id="PatientsPage">
            <a class="nav-link active" href="#">Patients</a>
          </li>
          <li class="nav-item">
            <a class="nav-link " href="./Services.php">Services</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./About.php">About Us</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./Contact.php">Contact Us</a>
          </li>
        </ul>

        <div id="userDetaliContainer" style="display: flex; align-items: center; justify-content: center;">
          <div style="width: 40px; height: 40px; background-color: #fff; border-radius: 50%; margin-right: 5px;">
            <img src="<?php echo $userImg;?>" alt="user" style="width: 100%; height: 100%;">
          </div>
          <h6 id="userName" data-bs-toggle="modal" data-bs-target="#profileModal" style="margin-right: 8px; margin-top: 5px; cursor: pointer"></h6>
          <ion-icon data-bs-toggle="modal" data-bs-target="#profileModal" name="chevron-down-outline"
            style="font-size: 15px; margin-right: 5px; cursor: pointer"></ion-icon>
        </div>
      </div>
    </div>
    </div>
  </nav>
    <div class="p-5">

        <div
            style="display: flex; flex-direction: row; align-items: center; justify-content: space-between; margin-right: 5%;">
            <p style="color: #004861; font-size: 26px; font-weight: 600;">List Of Patients:</p>
        </div>

        <div class="mt-4" style="width: 90%; margin-left: 3%;">
            <input class="form-control me-2 p-2" style=" box-shadow: 0px 0px 5px -3px #000;" type="search" id="search" placeholder="Search Patients" aria-label="Search">
        </div>

        <div class="mt-4">
            <table class="table" style="width: 95%;">
                <thead>
                    <tr>
                        <th style="color: #00A3C8;">PatientId</th>
                        <th style="color: #00A3C8;">Name</th>
                        <th style="color: #00A3C8;">Email</th>
                        <th style="color: #00A3C8;">Doctor</th>
                        <th style="color: #00A3C8;">Disease</th>
                        <th style="color: #00A3C8;">Date</th>
                        <th style="color: #00A3C8;">Time</th>
                        <th id="edit" style="color: #00A3C8;">Edit</th>
                        <th id="delete" style="color: #00A3C8;">Delete</th>
                    </tr>
                </thead>
                <?php
                include "../componants/connection.php";
                ?>
                
                <tbody id="tableData">
                </tbody>
                
            </table>

        </div>
    </div>

        <!-- footer -->
        <footer class="" style="background-color: #004861;">
            <div class="footer-container d-grid text-white-50 container py-4">
                <div class="py-3 d-flex flex-column">
                    <h1>MedHelp</h1>
                    <p class="pt-4" style="padding-right: 15%;">Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        Distinctio aperiam ad quisquam voluptate aut ab?</p>
                </div>
                <div class="py-3 d-flex flex-column">
                    <h4>Quick Links</h4>
                    <a href="./index.php" class="text-white-50 py-1 w-50">Home</a>
                    <!-- <a href="#" class="text-white-50 py-1 w-50">Dashboard</a> -->
                    <a href="#" class="text-white-50 py-1 w-50">Services</a>
                    <a href="./About.php" class="text-white-50 py-1 w-50">About us</a>
                    <a href="./Contact.php" class="text-white-50 py-1 w-50">Contact us</a>
                </div>
                <div class="py-3">
                    <h4>Contact us</h4>
                    <p><i class="bi-pin-fill"></i> 401-Shreeram complex, Ahmedabad - 392324</p>
                    <p><i class="bi-telephone"></i> +91 9876543210</p>
                    <p><i class="bi-envelope"></i> medhelp@gmail.com</p>
                </div>
                <div class="py-3">
                    <a href="#" class="fa fa-instagram mx-2 bg-gradient p-2 text-white"></a>
                    <a href="#" class="fa fa-facebook-official mx-2 bg-gradient p-2 text-white"></a>
                    <a href="#" class="fa fa-linkedin mx-2 bg-gradient p-2 text-white"></a>
                    <p><sup>©</sup>Do not sell or share My Professional information.</p>
                </div>
            </div>
        </footer>

    <!-- EditModal -->
    <div class="modal fade mt-3" id="example2Modal" tabindex="-1" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <form id="editForm" action="../componants/update.php?table=patients" method="post" class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="exampleModalLabel" style="color: #00A3C8;"><span
                            style="font-size: 16px; color: #b2b2b2; letter-spacing: 1;" id="editModelIdText"
                            name="editId">#1</span><br /><span id="editModelNameText">Tarakh Mehta</span></h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="p-1">
                        <div class="mb-1" style="display: flex; flex-direction: row; align-items: center;">
                            <label for="exampleFormControlInput1" class="form-label mt-2">Name:</label>
                            <input type="text" class="form-control ms-2" id="editModelInputName" name="editName">
                        </div>
                    </div>

                    <div class="p-1">
                        <div class="mb-1" style="display: flex; flex-direction: row; align-items: center;">
                            <label for="exampleFormControlInput1" class="form-label mt-2">Email:</label>
                            <input type="email" class="form-control ms-2" id="editModelInputEmail" name="editEmail">
                        </div>
                    </div>

                    <div class="p-1">
                        <div class="mb-1" style="display: flex; flex-direction: row; align-items: center;">
                            <label for="exampleFormControlInput1" class="form-label mt-2"
                                style="color: #004861;">Doctor:</label>
                            <div class="dropdown ms-4">
                                <select name="editDoctor" id="editModelInputDoc"
                                    style="padding: 10px; border-radius: 6px; border: 1px solid #dee2e6;">
                                    <?php
                                        $sql = "SELECT * FROM `tblDoctors`";
                                        $result = mysqli_query($conn, $sql) or die("Unscuccessfull");
                                        if(mySqli_num_rows($result) > 0){
                                            while($row = mysqli_fetch_assoc($result)){
                                    ?>
                                    <option value="<?php echo $row['name']; ?>">
                                        <?php echo $row['name']; ?>
                                    </option>
                                    <?php } } ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="p-1">
                        <div class="mb-1" style="display: flex; flex-direction: row; align-items: center;">
                            <label for="exampleFormControlInput1" class="form-label mt-2">Disease:</label>
                            <input type="text" class="form-control ms-2" id="editModelInputdisease" name="editDisease">
                        </div>
                    </div>


                    <div class="p-1">
                        <div class="mb-1" style="display: flex; flex-direction: row; align-items: center;">
                            <label for="exampleFormControlInput1" class="form-label title">Date: </label>
                            <input type="Date" class="form-control ms-2" name="editDatePicker" id="editDatePicker">
                        </div>
                    </div>

                    <div class="p-1">
                        <div class="mb-1" style="display: flex; flex-direction: row; align-items: center;">
                            <label for="exampleFormControlInput1" class="form-label title">Time: </label>
                            <input type="time" class="form-control ms-2" format='hh:mm:ss a' name="editTimePicker"
                                id="editTimePicker">
                        </div>
                    </div>




                    <input type="number" class="form-control ms-2" id="editModelIdInput" name="editId" hidden>

                </div>
                <div id='showErr' class="alert alert-danger ms-3 me-3" style="display: none;" role="alert">
                    Please enter valid value for all the fields for update appointment
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="button" class="btn" style="background-color: #00A3C8; color: #fff;"
                        onclick="EditPetfun()">Save
                        changes</button>
                </div>
            </form>
        </div>
    </div>

    <!-- ViewModal -->
  <div class="modal fade mt-2" id="profileModal" tabindex="-1" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h1 class="modal-title fs-5" id="exampleModalLabel" style="color: #00A3C8;"><span
              style="font-size: 16px; color: #b2b2b2; letter-spacing: 1;" id="viewModelIdText">#Profile</span><br />
            <span id="viewModelNameText" style="margin-top: -10px">
              <?php echo $userName; ?>
            </span></h1>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <div style="display: flex; align-items: center; justify-content: space-around;">
            <div>
                <img src="<?php echo $userImg; ?>" alt="" style="width: 100%; height: 150px;">
            </div>
            <div>
                <div class="p-2">
                    <p><span style="color: #004861; font-weight: 800;">Name: </span> <span
                        id="viewModelEmailText"><?php echo $userName; ?></span></p>
                    <p><span style="color: #004861; font-weight: 800;">Email: </span> <span
                            id="viewModelEmailText"><?php echo $userEmail; ?></span></p>
                    <p><span style="color: #004861; font-weight: 800;">password: </span> <span
                            id="viewModelCreatedText"><?php echo $userpass; ?></span></p>
                </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn" style="padding: 8px 20px; background-color: #fff; color: red; border: 1px solid red; border-radius: 6px; margin-left: 48px;" onclick="logoutBtn()">Logout</button>
     
        </div>
      </div>
    </div>
  </div>

    <script>
        const userName = "<?php echo $userName; ?>";
        const userType = "<?php echo $userType; ?>";
        const userId = "<?php echo $Id; ?>";

        if (userType != "receptionist" && userType != "doctor" && userType != "user"){
            location.href = "./LoginPage.php";
        }
        else {
      document.getElementById("userName").innerText = userName;
        }
        
        if (userType == "receptionist"){
            document.getElementById("deshborad").hidden = true;
            document.getElementById("appoinment").hidden = false;
            document.getElementById("PatientsPage").hidden = false;
            document.getElementById("edit").hidden = false;
            document.getElementById("delete").hidden = false;
        }
        else if(userType == "doctor"){
            document.getElementById("deshborad").hidden = true;
            document.getElementById("appoinment").hidden = true;
            document.getElementById("PatientsPage").hidden = false;
            document.getElementById("edit").hidden = true;
            document.getElementById("delete").hidden = true;
        }

        $("#search").keyup(function(){
            let search = $("#search").val();
            pageLoad(search)
        })

        pageLoad();
        function pageLoad(search){

            if (userType == "doctor") {
                $.ajax({
                    url: '../componants/select.php',
                    method: 'post',
                    data: { 'table': "patientsSimple", "search" : search, "user": userName},
                    success: function (result) {
                        $("#tableData").html(result)
                    }
                })
            }
            else if (userType == "receptionist") {
                $.ajax({
                    url: '../componants/select.php',
                    method: 'post',
                    data: { 'table': "patients", "search" : search},
                    success: function (result) {
                        $("#tableData").html(result)
                    }
                })
            }
        }

        function editViewBtn(e) {
            let doc = e.parentNode.firstElementChild.nextElementSibling.nextElementSibling.nextElementSibling.textContent.trim();
            let time = e.parentNode.firstElementChild.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling.textContent.trim();
            document.getElementById('editModelIdText').innerText = '#' + e.parentNode.firstElementChild.textContent.trim();
            document.getElementById('editModelIdInput').value = e.parentNode.firstElementChild.textContent.trim();
            document.getElementById('editModelNameText').innerText = e.parentNode.firstElementChild.nextElementSibling.textContent.trim();
            document.getElementById('editModelInputName').value = e.parentNode.firstElementChild.nextElementSibling.textContent.trim();
            document.getElementById('editModelInputEmail').value = e.parentNode.firstElementChild.nextElementSibling.nextElementSibling.textContent.trim();
            document.getElementById('editModelInputDoc').value = doc;
            document.getElementById('editModelInputdisease').value = e.parentNode.firstElementChild.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling.textContent.trim();
            document.getElementById('editDatePicker').value = e.parentNode.firstElementChild.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling.textContent.trim();
            console.log(time)
            if (time.slice(time.length - 2, time.length) == 'pm') {
                document.getElementById('editTimePicker').value = (parseInt(time.slice(0, 2)) + 12) + ":" + time.slice(3, 5);
            }
            else {
                document.getElementById('editTimePicker').value = time.slice(0, 2) + ":" + time.slice(3, 5);
            }
        }

        function EditPetfun() {
            let name = document.getElementById('editModelInputName').value;
            let email = document.getElementById('editModelInputEmail').value;
            let doctor = document.getElementById('editModelInputDoc').value;
            let disease = document.getElementById('editModelInputdisease').value;
            let editId = document.getElementById('editModelIdInput').value;
            let date = document.getElementById('editDatePicker').value;
            let time = document.getElementById('editTimePicker').value;
            let name_pattern = /[a-z A-Z]/i;
            let email_pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

            if (name.match(name_pattern) && email.match(email_pattern) && disease.match(name_pattern)) {
                document.getElementById('showErr').style.display = "none";
                $.ajax({
                    url: '../componants/update.php',
                    method: 'post',
                    data: { 'table': "patients", "editId": editId, "editName": name, "editEmail": email, "editDoctor" : doctor, "editDisease" : disease, "editDatePicker" : date, "editTimePicker" :time},
                    success: function (result) {
                        pageLoad();
                        $('#example2Modal').modal('toggle');
                    }
                })
            }
            else {
                document.getElementById('showErr').style.display = "block";
            }
        }

        function deleteBtn(e) {
            let id = e.parentNode.firstElementChild.textContent.trim();
            $.ajax({
                url: '../componants/delete.php',
                method: 'post',
                data: { 'table': "patients", "id": id },
                success: function (result) {
                    pageLoad();
                }
            })
        }

        function logoutBtn(){
        $.ajax({
            url: '../componants/logout.php',
            method: 'post',
            success: function (result) {
                if(result == "done"){
                    $('#profileModal').modal('toggle');
                    location.replace("./LoginPage.php")
                }
            }
        })
    }

    function appoinmentLink() {
            if (userName == "") {
        location.href = "./LoginPage.php";
      }
      else {
        location.href = "./AppointmentPage.php";
      }
    }
    </script>
</body>

</html>