<?php
        include "../componants/connection.php";

        session_start();
        if(isset($_SESSION["userId"])){
            $Id =  $_SESSION["userId"];
        }
        else{
            $Id = "";
            $userName = "";
            $userImg = "";
            $userpass = "";
            $userType = "";
            $userEmail = "";
        }
        
        if($Id != ""){
          $sql = "SELECT * FROM `tblRegister` WHERE `id`=$Id";
          $result = mysqli_query($conn, $sql) or die("Unscuccessfull");
          if(mySqli_num_rows($result) == 1){
             while($row = mysqli_fetch_assoc($result)){
                $userName = $row['name'];
                $userImg = $row['picture'];
                $userpass = $row['password'];
                $userType = $row['type'];
                $userEmail = $row['email'];
             }
          }
        }
       
    
    ?>
<body>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>MedHelp</title>
  <link rel="stylesheet" href="../css/AdminDeshborad.css">
  <link rel="stylesheet" href="../css/navbar.css">

  <!-- bootstrap -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm"
    crossorigin="anonymous"></script>

  <!-- Chart -->
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

  <!-- icons -->
  <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
  <script nomodule src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.js"></script>

    <!-- jquery-->
    <script src="../js/jquery.js"></script>

</head>
<nav class="navbar Fixed-top navbar-expand-lg headerNav">
    <div class="container-fluid">
      <a class="navbar-brand" href="./index.php" style="color: #00A3C8;">
        <h2>MedHelp</h2>
      </a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0 ms-5">
          <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="./index.php">Home</a>
          </li>
          <li class="nav-item" id="deshborad">
            <a class="nav-link" href="#">Dashboard</a>
          </li>
          <li class="nav-item" id="appoinment">
            <button class="nav-link" onclick="appoinmentLink()">Appoinment</button>
          </li>
          <li class="nav-item" id="PatientsPage">
            <a class="nav-link" href="./PatientsPage.php">Patients</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./Services.php">Services</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./About.php">About Us</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./Contact.php">Contact Us</a>
          </li>
        </ul>

        <div id="userDetaliContainer" style="display: flex; align-items: center; justify-content: center;">
          <div style="width: 40px; height: 40px; background-color: #fff; border-radius: 50%; margin-right: 5px;">
            <img src="<?php echo $userImg;?>" alt="user" style="width: 100%; height: 100%;">
          </div>
          <h6 id="userName" data-bs-toggle="modal" data-bs-target="#profileModal" style="margin-right: 8px; margin-top: 5px; cursor: pointer"></h6>
          <ion-icon data-bs-toggle="modal" data-bs-target="#profileModal" name="chevron-down-outline"
            style="font-size: 15px; margin-right: 5px; cursor: pointer"></ion-icon>
        </div>
      </div>
    </div>
    </div>
  </nav>


  <div class="sidebar">
    <button class="btnStat active" id="btnStat">Statistics</button>
    <button id="btnRec">Receptionist</button>
    <button id="btnDoc">Doctors</button>
    <button id="btnPat">Patients</button>
  </div>

  <div class="content mt-5" id="bodyPart">
  
  </div>

  <!-- ViewModal -->
  <div class="modal fade mt-2" id="profileModal" tabindex="-1" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h1 class="modal-title fs-5" id="exampleModalLabel" style="color: #00A3C8;"><span
              style="font-size: 16px; color: #b2b2b2; letter-spacing: 1;" id="viewModelIdText">#Profile</span><br />
            <span id="viewModelNameText" style="margin-top: -10px">
              <?php echo $userName; ?>
            </span></h1>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <div style="display: flex; align-items: center; justify-content: space-around;">
            <div>
                <img src="<?php echo $userImg; ?>" alt="" style="width: 100%; height: 150px;">
            </div>
            <div>
                <div class="p-2">
                    <p><span style="color: #004861; font-weight: 800;">Name: </span> <span
                        id="viewModelEmailText"><?php echo $userName; ?></span></p>
                    <p><span style="color: #004861; font-weight: 800;">Email: </span> <span
                            id="viewModelEmailText"><?php echo $userEmail; ?></span></p>
                    <p><span style="color: #004861; font-weight: 800;">password: </span> <span
                            id="viewModelCreatedText"><?php echo $userpass; ?></span></p>
                </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn" style="padding: 8px 20px; background-color: #fff; color: red; border: 1px solid red; border-radius: 6px; margin-left: 48px;" onclick="logoutBtn()">Logout</button>
        
        </div>
      </div>
    </div>
  </div>

  <script>
       const userName = "<?php echo $userName; ?>";
    const userType = "<?php echo $userType; ?>";

    if(userType != "admin"){
      location.replace("./LoginPage.php")
    }

    document.getElementById("userName").innerText = userName;
    document.getElementById("deshborad").hidden = false;
    document.getElementById("appoinment").hidden = true;
    document.getElementById("PatientsPage").hidden = true;

    const box = document.getElementById('bodyPart');
    let height = window.innerHeight;
    box.innerHTML =
      '<object width="100%" height=' + height + ' type="text/html" data="../componants/statistics.php"</object>';

    document.getElementById("btnStat").onclick = function () {
      document.getElementById("btnPat").classList.remove("active")
      document.getElementById("btnStat").classList.add("active")
      document.getElementById("btnRec").classList.remove("active")
      document.getElementById("btnDoc").classList.remove("active")
      const box = document.getElementById('bodyPart');
      let height = window.innerHeight;
      box.innerHTML =
        '<object width="100%" height=' + height + ' type="text/html" data="../componants/statistics.php"</object>';
    }

    document.getElementById("btnPat").onclick = function () {
      document.getElementById("btnStat").classList.remove("active")
      document.getElementById("btnPat").classList.add("active")
      document.getElementById("btnRec").classList.remove("active")
      document.getElementById("btnDoc").classList.remove("active")
      const box = document.getElementById('bodyPart');
      let height = window.innerHeight;
      box.innerHTML =
        '<object width="100%" height=' + height + ' type="text/html" data="../componants/patients.php"</object>';
    }

    document.getElementById("btnDoc").onclick = function () {
      document.getElementById("btnStat").classList.remove("active")
      document.getElementById("btnPat").classList.remove("active")
      document.getElementById("btnRec").classList.remove("active")
      document.getElementById("btnDoc").classList.add("active")
      const box = document.getElementById('bodyPart');
      let height = window.innerHeight;
      box.innerHTML =
        '<object width="100%" height=' + height + ' type="text/html" data="../componants/doctors.php"</object>';
    }

    document.getElementById("btnRec").onclick = function () {
      document.getElementById("btnStat").classList.remove("active")
      document.getElementById("btnPat").classList.remove("active")
      document.getElementById("btnDoc").classList.remove("active")
      document.getElementById("btnRec").classList.add("active")
      const box = document.getElementById('bodyPart');
      let height = window.innerHeight;
      box.innerHTML =
        '<object width="100%" height=' + height + ' type="text/html" data="../componants/receptionist.php"</object>';
    }
    function logoutBtn(){
        $.ajax({
            url: '../componants/logout.php',
            method: 'post',
            success: function (result) {
                if(result == "done"){
                    $('#profileModal').modal('toggle');
                    location.replace("./LoginPage.php")
                }
            }
        })
    }
  </script>

</body>

</html>