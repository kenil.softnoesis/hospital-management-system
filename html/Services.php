<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Services</title>
    <!-- css -->
    <link rel="stylesheet" href="../css/Index.css">
    <link rel="stylesheet" href="../css/About.css">
    <link rel="stylesheet" href="../css/Services.css">
    <!-- bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm"
        crossorigin="anonymous"></script>
    <!-- icon -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.2/font/bootstrap-icons.css">
    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.js"></script>
    
    <!-- animation-link -->
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

     <!-- jQuery -->
     <script src="../js/jquery.js"></script>

</head>
<?php
        include "../componants/connection.php";

        session_start();
        if(isset($_SESSION["userId"])){
            $Id =  $_SESSION["userId"];
        }
        else{
            $Id = "";
            $userName = "";
            $userImg = "";
            $userpass = "";
            $userType = "";
            $userEmail = "";
        }

        if($Id != ""){
            $sql = "SELECT * FROM `tblRegister` WHERE `id`=$Id";
            $result = mysqli_query($conn, $sql) or die("Unscuccessfull");
            if(mySqli_num_rows($result) == 1){
               while($row = mysqli_fetch_assoc($result)){
                  $userName = $row['name'];
                  $userImg = $row['picture'];
                  $userpass = $row['password'];
                  $userType = $row['type'];
                  $userEmail = $row['email'];
               }
            }
        }
      
    
    ?>
<body>
    <!-- header -->
    <div class="headerTop">
        <p class="headerTopText">Mon - Sat | 09:00AM - 10:00PM</p>
        <p class="headerTopText">+91 9874563210</p>
    </div>
    <nav class="navbar sticky-top navbar-expand-lg headerNav">
    <div class="container-fluid">
      <a class="navbar-brand" href="./index.php" style="color: #00A3C8;">
        <h2>MedHelp</h2>
      </a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0 ms-5">
          <li class="nav-item">
            <a class="nav-link " aria-current="page" href="./index.php">Home</a>
          </li>
          <li class="nav-item" id="deshborad">
            <a class="nav-link" href="./AdminDeshboradPage.php">Dashboard</a>
          </li>
          <li class="nav-item" id="appoinment">
            <button class="nav-link" onclick="appoinmentLink()">Appoinment</button>
          </li>
          <li class="nav-item" id="PatientsPage">
            <a class="nav-link" href="./PatientsPage.php">Patients</a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" href="#">Services</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./About.php">About Us</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./Contact.php">Contact Us</a>
          </li>
        </ul>
        <a href="./LoginPage.php"><button class="me-2 login" id="login"
            style="padding: 8px 20px; background-color: #fff; color: #00A3C8; border: 1px solid #00A3C8; border-radius: 6px; margin-left: 48px;">Log
            In</button></a>
            <a href="./Register.php"><button class="singup" id="signin"
          style="padding: 8px 20px; background-color: #00A3C8; color: #ffff; border: 0; border-radius: 6px;">Sign
          Up</button></a>

        <div id="userDetaliContainer" style="display: flex; align-items: center; justify-content: center;">
          <div style="width: 40px; height: 40px; background-color: #fff; border-radius: 50%; margin-right: 5px;">
            <img src="<?php echo $userImg;?>" alt="user" style="width: 100%; height: 100%;">
          </div>
          <h6 id="userName" data-bs-toggle="modal" data-bs-target="#profileModal" style="margin-right: 8px; margin-top: 5px; cursor: pointer"></h6>
          <ion-icon data-bs-toggle="modal" data-bs-target="#profileModal" name="chevron-down-outline"
            style="font-size: 15px; margin-right: 5px; cursor: pointer"></ion-icon>
        </div>
      </div>
    </div>
    </div>
  </nav>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 header">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 header_content">
                            <h5 style="color: #00A3C8; font-weight: 600;">INTRODUCING A NEW HEALTH CARE</h5>
                            <h1 style="font-size: 44px; font-weight: 700; letter-spacing: 5; color: #004861;">Get expert
                                treatment for Joint Pains, Surgery.</h1>
                            <p class="mt-4">
                            Like education healthcare is also need to be given importance. We need a cost-effective, high-quality health care system, guaranteeing health care to all of our people as a right. Take care of your health, that it may serve you to serve God.
                            </p>
                        
                        </div>
                        <div class="col-md-6 image-column">
                            <img src="../img/home-hero-img.jpg" alt="Your Image" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="bg-body-tertiary py-5">
        <div class="container">
            <h5 class="main_title pb-4 text-center" data-aos="fade-up">HOSPITAL SERVICES</h5>
            <h2 class="sub_title text-center" data-aos="fade-in">Our Healthcare Service</h2>
            <div class="service-sec py-5">
                <div class="service-box d-flex bg-white p-4 rounded-5" data-aos="fade-in">
                    <i class="bi bi-hospital fa-3x text-primary"></i>
                    <div class="px-3">
                        <h4>Medical Treatment</h4>
                        <p>Medical treatment can encompass a wide range of topics, including various conditions,
                            procedures, medications, and therapies. </p>
                    </div>
                </div>
                <div class="service-box d-flex bg-white p-4 rounded-5" data-aos="flip-up">
                    <i class="fa fa-ambulance fa-3x text-primary"></i>
                    <div class="px-3">
                        <h4>Emergency Help 24/7</h4>
                        <p>We provide round-the-clock care and support for COVID-19 patients, ensuring access to medical
                            attention whenever it's needed.</p>
                    </div>
                </div>
                <div class="service-box d-flex bg-white p-4 rounded-5" data-aos="fade-out">
                    <i class="bi bi-capsule-pill fa-3x text-primary"></i>
                    <div class="px-3">
                        <h4>Research Professionals</h4>
                        <p>These individuals conduct original research, design experiments, collect and analyze data,
                            and publish their findings in academic journals.</p>
                    </div>
                </div>
                <div class="service-box d-flex bg-white p-4 rounded-5" data-aos="fade-in">
                    <i class="bi bi-heart-pulse-fill fa-3x text-primary"></i>
                    <div class="px-3">
                        <h4>Check Ups</h4>
                        <p> These tests can help detect conditions like diabetes, heart disease, and kidney problems.
                            Regular blood pressure checks can help identify hypertension.</p>
                    </div>
                </div>
                <div class="service-box d-flex bg-white p-4 rounded-5" data-aos="fade-in">
                    <i class="bi bi-eye-fill fa-3x text-primary"></i>
                    <div class="px-3">
                        <h4>Eye Care</h4>
                        <p>Eye care professionals can conduct various screenings to assess your eye health. These
                            include tests for conditions like glaucoma, macular degeneration.</p>
                    </div>
                </div>
                <div class="service-box d-flex bg-white p-4 rounded-5" data-aos="fade-out">
                    <i class="fa fa-stethoscope fa-3x text-primary"></i>
                    <div class="px-3">
                        <h4>Diagnostics</h4>
                        <p>Diagnostic procedures play a crucial role in determining the cause of symptoms, guiding
                            treatment decisions, and monitoring the progress of medical conditions. </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="py-5">
        <div class="container">
            <h5 class="main_title pb-4 text-center" data-aos="fade-up">POPULAR MEDICAL SERVICES</h5>
            <h2 class="sub_title text-center" data-aos="fade-in">Benefit For Physical Mental and Virtual Care</h2>
            <div class="service-content d-grid text-center my-5">
                <div class="service_box bg-info-subtle rounded-4 shadow-sm" data-aos="fade-in">
                    <img src="../img/service-icon3.png" alt="corona" class="mt-4">
                    <h4>Cardiology</h4>
                    <div class="service_info text-center px-4 pb-4">
                        <p>At MedHelp, our top priority is the health and well-being of our community. In response to
                            the ongoing COVID-19 pandemic, we have established specialized treatment services to provide
                            exceptional care to patients affected by the virus.</p>
                    </div>
                </div>
                <div class="service_box bg-warning-subtle rounded-4 shadow-sm" data-aos="fade-down">
                    <img src="../img/service-icon6.png" alt="corona" class="mt-4">
                    <h4>Orthopedics</h4>
                    <div class="service_info text-center px-4 pb-4">
                        <p>Our orthopedic team is dedicated to providing world-class care for a wide range of orthopedic
                            conditions and injuries. Whether you're dealing with joint pain, sports injuries, or need
                            specialized orthopedic surgery, we are here to support your journey to better mobility and
                            comfort.</p>
                    </div>
                </div>
                <div class="service_box bg-warning-subtle rounded-4 shadow-sm" data-aos="fade-up">
                    <img src="../img/service-icon5.png" alt="corona" class="mt-4">
                    <h4>Neurology</h4>
                    <div class="service_info text-center px-4 pb-4">
                        <p>At MedHelp, we believe in providing hope and improving the lives of our patients living with
                            neurological conditions. Our commitment to excellence, patient-centered care, and innovation
                            ensures that you receive the best possible care on your journey to recovery.</p>
                    </div>
                </div>
                <div class="service_box bg-info-subtle rounded-4 shadow-sm" data-aos="fade-up">
                    <img src="../img/service-icon4.png" alt="corona" class="mt-4">
                    <h4>Pulmonary</h4>
                    <div class="service_info text-center px-4 pb-4">
                        <p>Our team of dedicated pulmonologists and healthcare professionals is here to provide
                            comprehensive care for a wide range of respiratory conditions. Whether you're dealing with a
                            chronic lung disease, respiratory infection, or need specialized pulmonary care, we are
                            committed to your well-being.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Subscribe section -->
    <div class="subscribe-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="section-title text-white">
                        <h1 class="pb-3" data-aos="fade-up">Subscribe to get Updated</h1>
                        <p class="text-center my-3">Sign up our newsletter to get update information, news or article
                            about medical </p>
                    </div>
                    <div class="subscribe-form" data-aos="fade-in">
                        <div>
                            <input type="Email" name="Email" placeholder="Enter Your Email" class="px-4" required>
                            <button type="submit" class="mt-2 border-0 text-white fw-medium">subscribe</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Professional Doctor -->
    <div class="bg-body-tertiary pt-5">
        <div class="container">
            <h5 class="main_title pb-4 text-center" data-aos="fade-up">MEET TO EXPERTS</h5>
            <h2 class="sub_title text-center" data-aos="fade-in">Professional Doctors</h2>
            <div class="doctor-section py-5">
                <div class="doctor-box text-center" data-aos="fade-in">
                    <img src="../img/doctor-team-1.jpg" alt="" class="img-fluid rounded-5">
                    <div class="doctor-info pt-4">
                        <h4 style="color: #004861;">Dr. Yash Donga</h4>
                        <p style="color: #00A3C8;">M.B.B.S.</p>
                    </div>
                </div>
                <div class="doctor-box text-center" data-aos="fade-out-up">
                    <img src="../img/doctor-team-2.jpg" alt="" class="img-fluid rounded-5">
                    <div class="doctor-info pt-4">
                        <h4 style="color: #004861;">Dr. Kaushik Rana</h4>
                        <p style="color: #00A3C8;">PSYCHOSOCIAL</p>
                    </div>
                </div>
                <div class="doctor-box text-center" data-aos="fade-out-up">
                    <img src="../img/doctor-team-3.jpg" alt="" class="img-fluid rounded-5">
                    <div class="doctor-info pt-4">
                        <h4 style="color: #004861;">Dr. Suresh Patel</h4>
                        <p style="color: #00A3C8;">CARDIOLOGY</p>
                    </div>
                </div>
                <div class="doctor-box text-center" data-aos="fade-up">
                    <img src="../img/doctor-team-4.jpg" alt="" class="img-fluid rounded-5">
                    <div class="doctor-info pt-4">
                        <h4 style="color: #004861;">Dr. Panakj Verma</h4>
                        <p style="color: #00A3C8;">PATHOLOGY</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- back to top -->
    <button type="button" title="Back to Top" class="text-center btn back-top" id="back-top">&#8593</button>

    <!-- footer -->
    <footer class="" style="background-color: #004861;">
        <div class="footer-container d-grid text-white-50 container py-4">
            <div class="py-3 d-flex flex-column">
                <h1>MedHelp</h1>
                <p class="pt-4" style="padding-right: 15%;">Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Distinctio aperiam ad quisquam voluptate aut ab?</p>
            </div>
            <div class="py-3 d-flex flex-column">
                <h4>Quick Links</h4>
                <a href="./index.php" class="text-white-50 py-1 w-50">Home</a>
                <!-- <a href="#" class="text-white-50 py-1 w-50">Dashboard</a> -->
                <a href="#" class="text-white-50 py-1 w-50">Services</a>
                <a href="./About.php" class="text-white-50 py-1 w-50">About us</a>
                <a href="./Contact.php" class="text-white-50 py-1 w-50">Contact us</a>
            </div>
            <div class="py-3">
                <h4>Contact us</h4>
                <p><i class="bi-pin-fill"></i> 401-Shreeram complex, Ahmedabad - 392324</p>
                <p><i class="bi-telephone"></i> +91 9876543210</p>
                <p><i class="bi-envelope"></i> medhelp@gmail.com</p>
            </div>
            <div class="py-3">
                <a href="#" class="fa fa-instagram mx-2 bg-gradient p-2 text-white"></a>
                <a href="#" class="fa fa-facebook-official mx-2 bg-gradient p-2 text-white"></a>
                <a href="#" class="fa fa-linkedin mx-2 bg-gradient p-2 text-white"></a>
                <p><sup>©</sup>Do not sell or share My Professional information.</p>
            </div>
        </div>
    </footer>

    <!-- ViewModal -->
  <div class="modal fade mt-2" id="profileModal" tabindex="-1" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h1 class="modal-title fs-5" id="exampleModalLabel" style="color: #00A3C8;"><span
              style="font-size: 16px; color: #b2b2b2; letter-spacing: 1;" id="viewModelIdText">#Profile</span><br />
            <span id="viewModelNameText" style="margin-top: -10px">
              <?php echo $userName; ?>
            </span></h1>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <div style="display: flex; align-items: center; justify-content: space-around;">
            <div>
                <img src="<?php echo $userImg; ?>" alt="" style="width: 100%; height: 150px;">
            </div>
            <div>
                <div class="p-2">
                    <p><span style="color: #004861; font-weight: 800;">Name: </span> <span
                        id="viewModelEmailText"><?php echo $userName; ?></span></p>
                    <p><span style="color: #004861; font-weight: 800;">Email: </span> <span
                            id="viewModelEmailText"><?php echo $userEmail; ?></span></p>
                    <p><span style="color: #004861; font-weight: 800;">password: </span> <span
                            id="viewModelCreatedText"><?php echo $userpass; ?></span></p>
                </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn" style="padding: 8px 20px; background-color: #fff; color: red; border: 1px solid red; border-radius: 6px; margin-left: 48px;" onclick="logoutBtn()">Logout</button>
         
        </div>
      </div>
    </div>
  </div>

    <!-- script -->
    <script>
        AOS.init();
    </script>

    <script>
         const userName = "<?php echo $userName; ?>";
    const userType = "<?php echo $userType; ?>";

    console.log(userType)

    if (userName == "") {
      document.getElementById("login").hidden = false;
      document.getElementById("signin").hidden = false;
      document.getElementById("userDetaliContainer").hidden = true;

    }
    else {
      document.getElementById("userName").innerText = userName;
      document.getElementById("login").hidden = true;
      document.getElementById("signin").hidden = true;
    }


    if (userType.trim() == "admin") {
      document.getElementById("deshborad").hidden = false;
      document.getElementById("appoinment").hidden = true;
      document.getElementById("PatientsPage").hidden = true;
    }
    else if (userType.trim() == "receptionist") {
      document.getElementById("deshborad").hidden = true;
      document.getElementById("appoinment").hidden = false;
      document.getElementById("PatientsPage").hidden = false;
    }
    else if (userType.trim() == "doctor") {
      document.getElementById("deshborad").hidden = true;
      document.getElementById("appoinment").hidden = true;
      document.getElementById("PatientsPage").hidden = false;
    }
    else {
      document.getElementById("deshborad").hidden = true;
      document.getElementById("appoinment").hidden = false;
      document.getElementById("PatientsPage").hidden = true;
    }


        let myButton = document.getElementById("back-top");
        window.onscroll = function () {
            scrollFunction();
        };
        function scrollFunction() {
            if (
                document.body.scrollTop > 200 ||
                document.documentElement.scrollTop > 400
            ) {
                myButton.style.display = "block";
            } else {
                myButton.style.display = "none";
            }
        }
        myButton.addEventListener("click", backToTop);
        function backToTop() {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
        }

        function appoinmentLink() {
            if (userName == "") {
        location.href = "./LoginPage.php";
      }
      else {
        location.href = "./AppointmentPage.php";
      }
    }

    function logoutBtn(){
        $.ajax({
            url: '../componants/logout.php',
            method: 'post',
            success: function (result) {
                if(result == "done"){
                    $('#profileModal').modal('toggle');
                    location.replace("./LoginPage.php")
                }
            }
        })
    }
    </script>
</body>

</html>