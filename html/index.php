<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>MedHelp</title>

  <!-- jQuery -->
  
  <script src="../js/jquery.js"></script>

  <!-- css -->
  <link rel="stylesheet" href="../css/navbar.css">
  <link rel="stylesheet" href="../css/Index.css">

  <!-- bootstrap -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet"
      integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm"
      crossorigin="anonymous"></script>
  
      <!-- icon -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="icon" href="/img/doctor-icon.png">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.2/font/bootstrap-icons.css">
    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.js"></script>
    
  <!-- animation-link -->
  <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
  <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

</head>
<?php
        include "../componants/connection.php";

        session_start();
        if(isset($_SESSION["userId"])){
            $Id =  $_SESSION["userId"];
        }
        else{
            $Id = "";
            $userName = "";
            $userImg = "";
            $userpass = "";
            $userType = "";
            $userEmail = "";
        }
        

        if($Id !=  ""){
            $sql = "SELECT * FROM `tblRegister` WHERE `id`=$Id";
            $result = mysqli_query($conn, $sql) or die("Unscuccessfull");
            if(mySqli_num_rows($result) == 1){
               while($row = mysqli_fetch_assoc($result)){
                  $userName = $row['name'];
                  $userImg = $row['picture'];
                  $userpass = $row['password'];
                  $userType = $row['type'];
                  $userEmail = $row['email'];
               }
            }
        }
    
    ?>
<body class="bg-ligh">
    
  <div class="headerTop">
    <p class="headerTopText">Mon - Sat | 09:00AM - 10:00PM</p>
    <p class="headerTopText">+91 9874563210</p>
  </div>
  <nav class="navbar sticky-top navbar-expand-lg headerNav">
    <div class="container-fluid">
      <a class="navbar-brand" href="#" style="color: #00A3C8;">
        <h2>MedHelp</h2>
      </a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0 ms-5">
          <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="#">Home</a>
          </li>
          <li class="nav-item" id="deshborad">
            <a class="nav-link" href="./AdminDeshboradPage.php">Dashboard</a>
          </li>
          <li class="nav-item" id="appoinment">
            <button class="nav-link" onclick="appoinmentLink()">Appoinment</button>
          </li>
          <li class="nav-item" id="PatientsPage">
            <a class="nav-link" href="./PatientsPage.php">Patients</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./Services.php">Services</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./About.php">About Us</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./Contact.php">Contact Us</a>
          </li>
        </ul>
        <a href="./LoginPage.php"><button class="me-2 login" id="login"
            style="padding: 8px 20px; background-color: #fff; color: #00A3C8; border: 1px solid #00A3C8; border-radius: 6px; margin-left: 48px;">Log
            In</button></a>
        <a href="./Register.php"><button class="singup" id="signin"
          style="padding: 8px 20px; background-color: #00A3C8; color: #ffff; border: 0; border-radius: 6px;">Sign
          Up</button></a>

        <div id="userDetaliContainer" style="display: flex; align-items: center; justify-content: center;">
          <div style="width: 40px; height: 40px; background-color: #fff; border-radius: 50%; margin-right: 5px;">
            <img src="<?php echo $userImg; ?>" id="userImg" alt="user" style="width: 100%; height: 100%;">
          </div>
          <h6 id="userName" data-bs-toggle="modal" data-bs-target="#profileModal" style="margin-right: 8px; margin-top: 5px; cursor: pointer"></h6>
          <ion-icon data-bs-toggle="modal" data-bs-target="#profileModal" name="chevron-down-outline"
            style="font-size: 15px; margin-right: 5px cursor: pointer"></ion-icon>
        </div>
      </div>
    </div>
    </div>
  </nav>

  
  <div class="container-fluid">
    <div class="row">
        <div class="col-md-12 header">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 header_content">
                        <h5 style="color: #00A3C8; font-weight: 600;">INTRODUCING A NEW HEALTH CARE</h5>
                        <h1 style="font-size: 44px; font-weight: 700; letter-spacing: 5; color: #004861;">Bring
                            Loving <br />Care to Health Care.</h1>
                        <p class="mt-4">
                            Like education healthcare is also need to be given importance. We need a cost-effective, high-quality health care system, guaranteeing health care to all of our people as a right. Take care of your health, that it may serve you to serve God.
                        </p>
                       
                    </div>
                    <div class="col-md-6 image-column mt-3">
                        <img src="../img/home-hero-img.jpg" alt="Your Image" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- ------------------section 2------------------- -->
<div class="bg-body py-5">
    <div class="sec2-container container mb-5">
        <h2 class="text-center mb-5 sub_title">What Special in MedHelp</h2>
        <div class="row d-grid provide_sec">
            <div class="provide_content bg-body-secondary shadow-sm rounded-5" data-aos="fade-in">
                <div class="pt-4">
                    <img src="../img/ambulance-icon.png" alt="" class="img-fluid">
                </div>
                <div class="text-center">
                    <h5 class="py-3">Medical Counselling</h5>
                    <p style="line-height: 170%;">Our hospital is equipped with a dedicated team of healthcare
                        professionals, including experienced physicians, nurses.
                    </p>
                </div>
            </div>
            <div class="provide_content bg-primary-subtle shadow-sm rounded-5" data-aos="fade-up">
                <div class="pt-4">
                    <img src="../img/equipment-icon.png" alt="" class="img-fluid">
                </div>
                <div class="text-center">
                    <h5 class="py-3">Newest Equipment</h5>
                    <p style="line-height: 170%;">We are proud to introduce our newest state-of-the-art equipment,
                        designed to enhance the accuracy, efficiency.
                    </p>
                </div>
            </div>
            <div class="provide_content bg-body-secondary shadow-sm rounded-5" data-aos="fade-down">
                <div class="pt-4">
                    <img src="../img/doctor-icon.png" alt="" class="img-fluid">
                </div>
                <div class="text-center">
                    <h5 class="py-3">Professional Doctor</h5>
                    <p style="line-height: 170%;">Our team of healthcare professionals includes highly skilled
                        doctors, specialists, and support staff with years of experience.
                    </p>
                </div>
            </div>
            <div class="provide_content bg-primary-subtle shadow-sm rounded-5" data-aos="fade-up">
                <div class="pt-4">
                    <img src="../img/service-icon.png" alt="" class="img-fluid">
                </div>
                <div class="text-center">
                    <h5 class="py-3">Best Service</h5>
                    <p style="line-height: 170%;">We provide personalized treatment plans tailored to your specific
                        condition, which may include non-surgical options.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- --------------About us Section------------ -->
<div style="background: #F8F8FF" class="py-5 mb-5">
    <div class="container-fluid">
        <div class="d-grid about_sec">
            <div class="about_img mx-4" data-aos="fade-in">
                <img src="../img/doctor-img.png" alt="docotr" class="bg-body shadow-sm"
                    style="border-top-right-radius: 30%; border-bottom-left-radius: 7%;">
            </div>
            <div class="about_content mx-5">
                <div class="about_info" data-aos="fade-up">
                    <h5 class="main_title pb-4" data-aos="fade-up">ABOUT</h5>
                    <h2 class="sub_title" data-aos="fade-in">Consult a doctor at any time, Anywhere</h2>
                    <p class="py-4" style="padding-right: 5%;">At MedHelp, we envision a healthcare
                        system that is Recursively Change Ownersh efficient, patient-centered, and accessible to
                        all. We are committed to simplifying hospital management, reducing operational costs, and
                        improving the overall quality of healthcare services.</p>
                </div>
                <div class="time_schedule bg-body rounded-5 py-2 shadow" data-aos="fade-out-down">
                    <h5 class="text-center py-2">: Open Hours :</h5>
                    <div class="timing">
                        <h6 class="d-flex">
                            <p class="">Monday :
                            <p class="">9:00 - 5:00</p>
                            </p>
                        </h6>
                        <h6 class="d-flex">
                            <p class="">Tuesday :
                            <p class="">9:00 - 5:00</p>
                            </p>
                        </h6>
                        <h6 class="d-flex">
                            <p class="">Wednesday :
                            <p class="">9:00 - 5:00</p>
                            </p>
                        </h6>
                        <h6 class="d-flex">
                            <p class="">Thursday :
                            <p class="">9:00 - 5:00</p>
                            </p>
                        </h6>
                        <h6 class="d-flex">
                            <p class="">Friday :
                            <p class="">9:00 - 5:00</p>
                            </p>
                        </h6>
                        <h6 class="d-flex" style="justify-content: space-evenly;">
                            <p class="">Saturday :
                            <p class="">9:00 - 5:00</p>
                            </p>
                        </h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Book Appointment -->
<div class="py-5 bg-body">
    <div class="container-fluid">
        <div class="d-grid appointment_sec">
            <div class="appointment_content mx-5">
                <div class="appointment_info" data-aos="fade-down">
                    <div>
                        <h5 class="main_title pb-4" data-aos="fade-up">APPOINTMENT</h5>
                        <h2 class="sub_title" data-aos="fade-in">Book an Appointment today, We are online</h2>
                        <p class="py-4">We at MedHelp every case completely centered around causing your to
                            conquered any specialist method, with incredible responsibility and simple recuperation.
                            We've combined a new kind of doctor's experience that blends the highest level of health
                            care with exceptional service. People can enroll membership by paying only an annual
                            membership fee of just $129. Your health and well-being are our top priorities. We make
                            it easy for you to schedule appointments with our experienced medical professionals,
                            ensuring that you receive the care you need in a timely and convenient manner.
                        </p>
                        <h5 class="pb-3" style="color: #004861;">What to Expect During Your Visit</h5>
                        <p><i class="bi bi-check-circle-fill h5 text-primary d-inline pe-2"></i>Minimal wait times,
                            thanks to our appointment scheduling system.</p>
                        <p><i class="bi bi-check-circle-fill h5 text-primary d-inline pe-2"></i>Personalized care
                            and attention from our medical professionals.</p>
                        <p><i class="bi bi-check-circle-fill h5 text-primary d-inline pe-2"></i>Prompt and efficient
                            service.</p>
                        <p><i class="bi bi-check-circle-fill h5 text-primary d-inline pe-2"></i>A comfortable and
                            welcoming healthcare environment.</p>
                    </div>
                </div>
            </div>
            <div class="appointment_img mx-4"  data-aos="fade-up">
                <img src="../img/appointment-img.png" alt="docotr" class="h-100"
                    style="filter: drop-shadow(rgba(0, 0, 0, 0.16) 0px 1px 4px);">
            </div>
        </div>
    </div>
</div>

<!-- medical Services -->

<div class="py-5">
    <div class="container">
        <h5 class="main_title pb-4 text-center" data-aos="fade-up">POPULAR MEDICAL SERVICES</h5>
        <h2 class="sub_title text-center" data-aos="fade-in">Benefit For Physical Mental and Virtual Care</h2>
        <div class="service-content d-grid text-center my-5">
            <div class="service_box bg-info-subtle rounded-4 shadow-sm" data-aos="fade-in-up">
                <img src="../img/service-icon1.png" alt="corona" class="mt-4">
                <h4>Covid-19</h4>
                <div class="service_info text-center px-4 pb-4">
                    <p>At MedHelp, our top priority is the health and well-being of our community. In response to
                        the ongoing COVID-19 pandemic, we have established specialized treatment services to provide
                        exceptional care to patients affected by the virus.</p>
                </div>
            </div>
            <div class="service_box bg-warning-subtle rounded-4 shadow-sm" data-aos="fade-out-down">
                <img src="../img/service-icon6.png" alt="corona" class="mt-4">
                <h4>Orthopedics</h4>
                <div class="service_info text-center px-4 pb-4">
                    <p>Our orthopedic team is dedicated to providing world-class care for a wide range of orthopedic
                        conditions and injuries. Whether you're dealing with joint pain, sports injuries, or need
                        specialized orthopedic surgery, we are here to support your journey to better mobility and
                        comfort.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Subscribe section -->
<div class="subscribe-area ">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="section-title text-white">
                    <h1 class="pb-3" data-aos="fade-in">Subscribe to get Updated</h1>
                    <p class="text-center my-3">Sign up our newsletter to get update information, news or
                        article
                        about medical </p>
                </div>
                <div class="subscribe-form" data-aos="fade-up">
                    <form action="#">
                        <input type="Email" name="Email" placeholder="Enter Your Email" class="px-4" required>
                        <button type="submit" class="mt-2 border-0 text-white fw-medium">subscribe</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- carousel -->
<div class="py-5 bg-body">
    <h5 class="main_title pb-4 text-center" data-aos="fade-up">GLOBAL SERVICES</h5>
    <h2 class="sub_title text-center" data-aos="fade-in">Best medical & health services provider</h2>
    <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-inner container">
            <div class="carousel-item active">
                <div class="d-grid mx-5 py-5 gap-5 text-center doc_info" style="place-items: center;">
                    <img src="../img/doctor-carousel-img1.jpg" alt="" class="img-thumbnail border-primary p-1"
                        style="border-radius: 50%; width: 125px; height: 125px;">
                    <p>I recently had the privilege of receiving medical care and I can't express how impressed I am
                        with their expertise, compassion, and commitment to patient care.</p>
                    <div>
                        <h4>Eshan Shah</h4>
                        <p>M.B.B.S.</p>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="d-grid mx-5 py-5 gap-5 text-center doc_info" style="place-items: center;">
                    <img src="../img/doctor-carousel-img2.png" alt="" class="img-thumbnail border-primary p-1"
                        style="border-radius: 50%; width: 125px; height: 125px;">
                    <p> I felt welcomed and well-cared for and their team were not only professional but also
                        genuinely caring and attentive. They took the time to listen to my concerns, answer my
                        questions, and explain the treatment plan in a way that I could easily understand.</p>
                    <div>
                        <h4>Mansi Parmar</h4>
                        <p>Pathology</p>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="d-grid mx-5 py-5 gap-5 text-center doc_info" style="place-items: center;">
                    <img src="../img/doctor-carousel-img3.jpg" alt="" class="img-thumbnail border-primary p-1"
                        style="border-radius: 50%; width: 125px; height: 125px;">
                    <p>medical knowledge and diagnostic skills are truly commendable. They conducted a thorough
                        examination, ordered the necessary tests, and provided a clear and accurate diagnosis.</p>
                    <div>
                        <h4>Harshad Gupta</h4>
                        <p>Surgery Specialist</p>
                    </div>
                </div>
            </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators"
            data-bs-slide="prev">
            <span class="carousel-control-prev-icon bg-primary" style="border-radius: 50%;"
                aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators"
            data-bs-slide="next">
            <span class="carousel-control-next-icon bg-primary" style="border-radius: 50%;"
                aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>
</div>

<!-- back to top -->
<button type="button" title="Back to Top" class="text-center btn back-top" id="back-top" data-aos="fade-in-down">&#8593</button>

<!-- footer -->
<footer class="" style="background-color: #004861;">
    <div class="footer-container d-grid text-white-50 container py-4">
        <div class="py-3 d-flex flex-column">
            <h1>MedHelp</h1>
            <p class="pt-4" style="padding-right: 15%;">Lorem ipsum dolor sit amet consectetur adipisicing elit.
                Distinctio aperiam ad quisquam voluptate aut ab?</p>
        </div>
        <div class="py-3 d-flex flex-column">
            <h4>Quick Links</h4>
            <a href="#" class="text-white-50 py-1 w-50">Home</a>
            <a href="#" class="text-white-50 py-1 w-50">Dashboard</a>
            <a href="./Services.html" class="text-white-50 py-1 w-50">Services</a>
            <a href="./About.html" class="text-white-50 py-1 w-50">About us</a>
            <a href="./Contact.html" class="text-white-50 py-1 w-50">Contact us</a>
        </div>
        <div class="py-3">
            <h4>Contact us</h4>
            <p><i class="bi-pin-fill"></i> 401-Shreeram complex, Ahmedabad - 392324</p>
            <p><i class="bi-telephone"></i> +91 9876543210</p>
            <p><i class="bi-envelope"></i> medhelp@gmail.com</p>
        </div>
        <div class="py-3">
            <a href="#" class="fa fa-instagram mx-2 bg-gradient p-2 text-white"></a>
            <a href="#" class="fa fa-facebook-official mx-2 bg-gradient p-2 text-white"></a>
            <a href="#" class="fa fa-linkedin mx-2 bg-gradient p-2 text-white"></a>
            <p><sup>©</sup>Do not sell or share My Professional information.</p>
        </div>
    </div>
</footer>


  <!-- ViewModal -->
  <div class="modal fade mt-2" id="profileModal" tabindex="-1" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h1 class="modal-title fs-5" id="exampleModalLabel" style="color: #00A3C8;"><span
              style="font-size: 16px; color: #b2b2b2; letter-spacing: 1;" id="viewModelIdText">#Profile</span><br />
            <span id="viewModelNameText" style="margin-top: -10px">
              <?php echo $userName; ?>
            </span></h1>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <div style="display: flex; align-items: center; justify-content: space-around;">
            <div>
                <img src="<?php echo $userImg; ?>" alt="" style="width: 100%; height: 150px;">
            </div>
            <div>
                <div class="p-2">
                    <p><span style="color: #004861; font-weight: 800;">Name: </span> <span
                        id="viewModelEmailText"><?php echo $userName; ?></span></p>
                    <p><span style="color: #004861; font-weight: 800;">Email: </span> <span
                            id="viewModelEmailText"><?php echo $userEmail; ?></span></p>
                    <p><span style="color: #004861; font-weight: 800;">password: </span> <span
                            id="viewModelCreatedText"><?php echo $userpass; ?></span></p>
                </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn" style="padding: 8px 20px; background-color: #fff; color: red; border: 1px solid red; border-radius: 6px; margin-left: 48px;" onclick="logoutBtn()">Logout</button>
        
        </div>
      </div>
    </div>
  </div>

  <script>
    AOS.init();
</script>

  <script>
    const userName = "<?php echo $userName; ?>";
    const userType = "<?php echo $userType; ?>";
    const userImg = "<?php echo $userImg; ?>";
 
        console.log(userName)

    if (userName == "") {
      document.getElementById("login").hidden = false;
      document.getElementById("signin").hidden = false;
      document.getElementById("userDetaliContainer").hidden = true;

    }
    else {
      document.getElementById("userName").innerText = userName;
      document.getElementById("login").hidden = true;
      document.getElementById("signin").hidden = true;
    }


    if (userType.trim() == "admin") {
      document.getElementById("deshborad").hidden = false;
      document.getElementById("appoinment").hidden = true;
      document.getElementById("PatientsPage").hidden = true;
    }
    else if (userType.trim() == "receptionist") {
      document.getElementById("deshborad").hidden = true;
      document.getElementById("appoinment").hidden = false;
      document.getElementById("PatientsPage").hidden = false;
    }
    else if (userType.trim() == "doctor") {
      document.getElementById("deshborad").hidden = true;
      document.getElementById("appoinment").hidden = true;
      document.getElementById("PatientsPage").hidden = false;
    }
    else {
      document.getElementById("deshborad").hidden = true;
      document.getElementById("appoinment").hidden = false;
      document.getElementById("PatientsPage").hidden = true;
    }

    let myButton = document.getElementById("back-top");
        window.onscroll = function () {
            scrollFunction();
        };
        function scrollFunction() {
            if (
                document.body.scrollTop > 200 ||
                document.documentElement.scrollTop > 400
            ) {
                myButton.style.display = "block";
            } else {
                myButton.style.display = "none";
            }
        }
        myButton.addEventListener("click", backToTop);
        function backToTop() {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
        }

    function appoinmentLink() {
        if (userName == "") {
        location.href = "./LoginPage.php";
      }
      else {
        location.href = "./AppointmentPage.php";
      }
    }

    function logoutBtn(){
        $.ajax({
            url: '../componants/logout.php',
            method: 'post',
            success: function (result) {
                if(result == "done"){
                    $('#profileModal').modal('toggle');
                    location.replace("./LoginPage.php")
                }
            }
        })
    }

  </script>
</body>

</html>
