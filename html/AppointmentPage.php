
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Appoinment</title>
    
    <link rel="stylesheet" href="../css/navbar.css">
    <link rel="stylesheet" href="../css/AppointmentPage.css">
    <link rel="stylesheet" href="../css/Index.css">

    <!-- bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm"
        crossorigin="anonymous"></script>

    <!-- jQuery -->
    <script src="../js/jquery.js"></script>

     <!-- icons -->
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.2/font/bootstrap-icons.css">
    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.js"></script>

</head>
<?php
        include "../componants/connection.php";

        session_start();
        if(isset($_SESSION["userId"])){
            $Id =  $_SESSION["userId"];
        }
        else{
            $Id = "";
            $userName = "";
            $userImg = "";
            $userpass = "";
            $userType = "";
            $userEmail = "";
        }
        
        if($Id != ""){
          $sql = "SELECT * FROM `tblRegister` WHERE `id`=$Id";
          $result = mysqli_query($conn, $sql) or die("Unscuccessfull");
          if(mySqli_num_rows($result) == 1){
             while($row = mysqli_fetch_assoc($result)){
                $userName = $row['name'];
                $userImg = $row['picture'];
                $userpass = $row['password'];
                $userType = $row['type'];
                $userEmail = $row['email'];
             }
          }
        }
    
    ?>
<body>
    <!-- header -->
    <div class="headerTop">
        <p class="headerTopText">Mon - Sat | 09:00AM - 10:00PM</p>
        <p class="headerTopText">+91 9874563210</p>
    </div>
    <nav class="navbar sticky-top navbar-expand-lg headerNav">
        <div class="container-fluid">
            <a class="navbar-brand" href="./HomePage.php" , style="color: #00A3C8;">
                <h2>MedHelp</h2>
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0 ms-5">
                    <li class="nav-item">
                        <a class="nav-link" href="./index.php">Home</a>
                    </li>
                    <li class="nav-item" id="deshborad" hidden>
                        <a class="nav-link" href="./AdminDeshboradPage.php">Dashboard</a>
                    </li>
                    <li class="nav-item" id="appoinment">
                        <button class="nav-link active" onclick="appoinmentLink()">Appoinment</button>
                    </li>
                    <li class="nav-item" id="PatientsPage" hidden>
                        <a class="nav-link" href="./PatientsPage.php">Patients</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="./Services.php">Services</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="./About.php">About Us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="./Contact.php">Contact Us</a>
                    </li>
                </ul>
                <div id="userDetaliContainer" style="display: flex; align-items: center; justify-content: center;">
          <div style="width: 40px; height: 40px; background-color: #fff; border-radius: 50%; margin-right: 5px;">
            <img src="<?php echo $userImg;?>" alt="user" style="width: 100%; height: 100%;">
          </div>
          <h6 id="userName" data-bs-toggle="modal" data-bs-target="#profileModal" style="margin-right: 8px; margin-top: 5px; cursor: pointer"></h6>
          <ion-icon data-bs-toggle="modal" data-bs-target="#profileModal" name="chevron-down-outline"
            style="font-size: 15px; margin-right: 5px; cursor: pointer"></ion-icon>
        </div>
            </div>
        </div>
        </div>
    </nav>

    <div class="p-5" style="margin-top: -20px">
        <h1 class="mt-3">Book Appointment</h1>
        <p style="height: 3px; background-color: #00A3C8;"></p>

        <div class="mt-5" style="width: 90%; margin-left: 5%;">
            <div class="row mb-3">
                <div class="col-md-6 mb-3">
                    <label for="exampleFormControlInput1" class="form-label title">First Name</label>
                    <input type="text" class="form-control" id="fname" name="fname" placeholder="Enter First Name">
                </div>
                <div class="col-md-6 mb-3">
                    <label for="exampleFormControlInput1" class="form-label title">Last Name</label>
                    <input type="text" class="form-control" id="lname" name="lname" placeholder="Enter Last Name">
                </div>
            </div>

            <div class="row mb-3">
                <div class="col-md-6">
                    <label for="exampleFormControlInput1" class="form-label title">Email Address</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="Enter Email Address">
                </div>
                <div class="col-md-6 mb-3">
                    <label for="exampleFormControlInput1" class="form-label title">Contact No</label>
                    <input type="text" class="form-control" id="phno" name="phno" placeholder="Enter Contact No">
                </div>
            </div>

            <div class="row mb-3">
                <div class="col-md-6 mb-3">
                    <label for="exampleFormControlInput1" class="form-label title">Doctor</label>
                    <br />
                    <select id="doctor" name="doctor"
                        style="padding: 7px 50px; background-color: #fff; border: 1px solid #dee2e6; border-radius: 6px;">
                        <?php

                            $sql = "SELECT * FROM `tblDoctors`";
                            $result = mysqli_query($conn, $sql) or die("Unscuccessfull");
                            if(mySqli_num_rows($result) > 0){
                                while($row = mysqli_fetch_assoc($result)){
                        ?>
                        <option value="<?php echo $row['name']; ?>">
                            <?php echo $row['name']; ?>
                        </option>
                        <?php } } ?>
                    </select>
                </div>
                <div class="col-md-6 mb-3">
                    <label for="exampleFormControlInput1" class="form-label title">Disease</label>
                    <input type="text" class="form-control" id="disease" name="disease" placeholder="Enter Disease">
                </div>
            </div>

            <div class="row mb-3">
                <div class="col-md-6 mb-3">
                    <label for="exampleFormControlInput1" class="form-label title">Date</label>
                    <input type="Date" class="form-control" name="datePicker" id="datePicker">
                </div>
                <div class="col-md-6 mb-3">
                    <label for="exampleFormControlInput1" class="form-label title">Time</label>
                    <input type="time" class="form-control" format='hh:mm:ss a' name="timePicker" id="timePicker" min="09:00" max="22:00">
                </div>
            </div>

            <div id='showErr' class="alert alert-danger ms-3 me-3" style="display: none;" role="alert">
                Please enter valid value for all the fields for book an appoinment
            </div>

            <div class="mt-5" style="display: flex; flex-direction: row; align-items: center; justify-content: center;">
                <button type="button" class="btn" style="width: 20%; border: 1px solid #00A3C8; color: #00A3C8;"
                    onclick="btnFormSubmit()">Book</button>
                <button type="button" class="btn btn-outline-secondary"
                    style="width: 20%; margin-left: 10%;" onclick="ClearBtn()">Clear</button>
            </div>
        </div>
    </div>
        <!-- footer -->
        <footer class="" style="background-color: #004861;">
        <div class="footer-container d-grid text-white-50 container py-4">
            <div class="py-3 d-flex flex-column">
                <h1>MedHelp</h1>
                <p class="pt-4" style="padding-right: 15%;">Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Distinctio aperiam ad quisquam voluptate aut ab?</p>
            </div>
            <div class="py-3 d-flex flex-column">
                <h4>Quick Links</h4>
                <a href="./index.php" class="text-white-50 py-1 w-50">Home</a>
                <a href="./Services.php" class="text-white-50 py-1 w-50">Services</a>
                <a href="./About.php" class="text-white-50 py-1 w-50">About us</a>
                <a href="./Contact.php" class="text-white-50 py-1 w-50">Contact us</a>
            </div>
            <div class="py-3">
                <h4>Contact us</h4>
                <p><i class="bi-pin-fill"></i> 401-Shreeram complex, Ahmedabad - 392324</p>
                <p><i class="bi-telephone"></i> +91 9876543210</p>
                <p><i class="bi-envelope"></i> medhelp@gmail.com</p>
            </div>
            <div class="py-3">
                <a href="#" class="fa fa-instagram mx-2 bg-gradient p-2 text-white"></a>
                <a href="#" class="fa fa-facebook-official mx-2 bg-gradient p-2 text-white"></a>
                <a href="#" class="fa fa-linkedin mx-2 bg-gradient p-2 text-white"></a>
                <p><sup>©</sup>Do not sell or share My Professional information.</p>
            </div>
        </div>
    </footer>

    <!-- ViewModal -->
  <div class="modal fade mt-2" id="profileModal" tabindex="-1" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h1 class="modal-title fs-5" id="exampleModalLabel" style="color: #00A3C8;"><span
              style="font-size: 16px; color: #b2b2b2; letter-spacing: 1;" id="viewModelIdText">#Profile</span><br />
            <span id="viewModelNameText" style="margin-top: -10px">
              <?php echo $userName; ?>
            </span></h1>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <div style="display: flex; align-items: center; justify-content: space-around;">
            <div>
                <img src="<?php echo $userImg; ?>" alt="" style="width: 100%; height: 150px;">
            </div>
            <div>
                <div class="p-2">
                    <p><span style="color: #004861; font-weight: 800;">Name: </span> <span
                        id="viewModelEmailText"><?php echo $userName; ?></span></p>
                    <p><span style="color: #004861; font-weight: 800;">Email: </span> <span
                            id="viewModelEmailText"><?php echo $userEmail; ?></span></p>
                    <p><span style="color: #004861; font-weight: 800;">password: </span> <span
                            id="viewModelCreatedText"><?php echo $userpass; ?></span></p>
                </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn" style="padding: 8px 20px; background-color: #fff; color: red; border: 1px solid red; border-radius: 6px; margin-left: 48px;" onclick="logoutBtn()">Logout</button>
     
        </div>
      </div>
    </div>
  </div>

   <!-- successModal -->
   <div class="modal fade mt-4" id="successModel" tabindex="-1" aria-labelledby="exampleModalLabel"
   aria-hidden="true">
   <div class="modal-dialog">
     <div class="modal-content">
       
       <div class="modal-body">
         
               <div class="p-2">
                   <img src="../img/GreenCheck.jpeg" alt="" style="width: 60%; height: 150px; margin-left: 20%;">
                   <h6 style="text-align: center;">Your appoinment is booked successfully</h6>
               </div>
       </div>
     </div>
   </div>
 </div>


    <script>
        const userName = "<?php echo $userName; ?>";
        const userType = "<?php echo $userType; ?>";
    
        if (userType != "receptionist" && userType != "user"){
            location.href = "./LoginPage.php"
        }

    else {
      document.getElementById("userName").innerText = userName;
    }

        if (userType == "receptionist"){
            document.getElementById("deshborad").hidden = true;
            document.getElementById("appoinment").hidden = false;
            document.getElementById("PatientsPage").hidden = false;
        }
        else if(userType == "user"){
            document.getElementById("deshborad").hidden = true;
            document.getElementById("appoinment").hidden = false;
            document.getElementById("PatientsPage").hidden = true;
        }

        function btnFormSubmit() {
            let fname = document.getElementById("fname").value;
            let lname = document.getElementById("lname").value;
            let email = document.getElementById("email").value;
            let phno = document.getElementById("phno").value;
            let doctor = document.getElementById("doctor").value;
            let disease = document.getElementById("disease").value;
            let date = document.getElementById("datePicker").value;
            let time = document.getElementById("timePicker").value;
            let name_pattern = /[a-z A-Z]/i;
            let email_pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

            if (fname.match(name_pattern) && lname.match(name_pattern) && email.match(email_pattern) && !isNaN(phno) && phno.length == 10 && disease.match(name_pattern) && date != "" && time != "") {
                document.getElementById('showErr').style.display = "none";
                $.ajax({
                    url: '../componants/insert.php',
                    method: 'post',
                    data: { 'table': "patients", "fname": fname, "lname": lname, "email": email, "phno": phno, "doctor": doctor, "disease": disease, "datePicker": date, "timePicker": time },
                    success: function (result) {
                        if(userType == "receptionist"){
                            location.replace('PatientsPage.php')
                        }
                        else{
                            document.getElementById("fname").value= "";
        document.getElementById("lname").value = "";
        document.getElementById("email").value = "";
        document.getElementById("phno").value = "";
        document.getElementById("disease").value = "";
        document.getElementById("datePicker").value = "";
        document.getElementById("timePicker").value = "";
        $('#successModel').modal('toggle');
                        }   
                    } 
                })
            }
            else {
                document.getElementById("showErr").style.display = "block"
            }
        }

        function appoinmentLink() {
            if (userName == "") {
                location.href = "./LoginPage.php";
            }
            else {
                location.href = "./AppointmentPage.php";
            }
        }
        function logoutBtn(){
        $.ajax({
            url: '../componants/logout.php',
            method: 'post',
            success: function (result) {
                if(result == "done"){
                    $('#profileModal').modal('toggle');
                    location.replace("./LoginPage.php")
                }
            }
        })
    }

    function ClearBtn(){
        document.getElementById("fname").value= "";
        document.getElementById("lname").value = "";
        document.getElementById("email").value = "";
        document.getElementById("phno").value = "";
        document.getElementById("disease").value = "";
        document.getElementById("datePicker").value = "";
        document.getElementById("timePicker").value = "";
    }
    </script>
</body>

</html>