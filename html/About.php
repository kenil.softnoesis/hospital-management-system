<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>About Us</title>
    <!-- css -->
    <link rel="stylesheet" href="../css/Index.css">
    <link rel="stylesheet" href="../css/About.css">
    <!-- bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm"
        crossorigin="anonymous"></script>
    <!-- icon -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.2/font/bootstrap-icons.css">
    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.js"></script>

     <!-- jQuery -->
     <script src="../js/jquery.js"></script>

    <!-- animation-link -->
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

</head>
<?php
        include "../componants/connection.php";

        session_start();
        if(isset($_SESSION["userId"])){
            $Id =  $_SESSION["userId"];
        }
        else{
            $Id = "";
            $userName = "";
            $userImg = "";
            $userpass = "";
            $userType = "";
            $userEmail = "";
        }
        
        if($Id != ""){
        $sql = "SELECT * FROM `tblRegister` WHERE `id`=$Id";
         $result = mysqli_query($conn, $sql) or die("Unscuccessfull");
         if(mySqli_num_rows($result) == 1){
            while($row = mysqli_fetch_assoc($result)){
               $userName = $row['name'];
               $userImg = $row['picture'];
               $userpass = $row['password'];
               $userType = $row['type'];
               $userEmail = $row['email'];
            }
         }
        }
    
    ?>
<body>
    <div class="headerTop">
        <p class="headerTopText">Mon - Sat | 09:00AM - 10:00PM</p>
        <p class="headerTopText">+91 9874563210</p>
    </div>
    <nav class="navbar sticky-top navbar-expand-lg headerNav">
    <div class="container-fluid">
      <a class="navbar-brand" href="./index.php" style="color: #00A3C8;">
        <h2>MedHelp</h2>
      </a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0 ms-5">
          <li class="nav-item">
            <a class="nav-link " aria-current="page" href="./index.php">Home</a>
          </li>
          <li class="nav-item" id="deshborad">
            <a class="nav-link" href="./AdminDeshboradPage.php">Dashboard</a>
          </li>
          <li class="nav-item" id="appoinment">
            <button class="nav-link" onclick="appoinmentLink()">Appoinment</button>
          </li>
          <li class="nav-item" id="PatientsPage">
            <a class="nav-link" href="./PatientsPage.php">Patients</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./Services.php">Services</a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" href="./About.php">About Us</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./Contact.php">Contact Us</a>
          </li>
        </ul>
        <a href="./LoginPage.php"><button class="me-2 login" id="login"
            style="padding: 8px 20px; background-color: #fff; color: #00A3C8; border: 1px solid #00A3C8; border-radius: 6px; margin-left: 48px;">Log
            In</button></a>
         <a href="./Register.php"><button class="singup" id="signin"
          style="padding: 8px 20px; background-color: #00A3C8; color: #ffff; border: 0; border-radius: 6px;">Sign
          Up</button></a>

        <div id="userDetaliContainer" style="display: flex; align-items: center; justify-content: center;">
          <div style="width: 40px; height: 40px; background-color: #fff; border-radius: 50%; margin-right: 5px;">
            <img src="<?php echo $userImg;?>" alt="user" style="width: 100%; height: 100%;">
          </div>
          <h6 id="userName" data-bs-toggle="modal" data-bs-target="#profileModal" style="margin-right: 8px; margin-top: 5px; cursor: pointer"></h6>
          <ion-icon data-bs-toggle="modal" data-bs-target="#profileModal" name="chevron-down-outline"
            style="font-size: 15px; margin-right: 5pxv"></ion-icon>
        </div>
      </div>
    </div>
    </div>
  </nav>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 header">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 header_content">
                            <h5 style="color: #00A3C8; font-weight: 600;">INTRODUCING A NEW HEALTH CARE</h5>
                            <h1 style="font-size: 44px; font-weight: 700; letter-spacing: 5; color: #004861;">Short
                                Story About <br />
                                Fovia Clinic Since 1999.</h1>
                            <p class="mt-5">
                            Like education healthcare is also need to be given importance. We need a cost-effective, high-quality health care system, guaranteeing health care to all of our people as a right. Take care of your health, that it may serve you to serve God.
                            </p>
                        </div>
                        <div class="col-md-6 image-column mt-3">
                        <img src="../img/home-hero-img.jpg" alt="Your Image" class="img-fluid">
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- about section container -->
    <div class="py-5">
        <div class="container">
            <div class="card mb-3" style="border: none;">
                <div class="row g-0">
                    <div class="col-md-6 py-4">
                        <div class="card-body" data-aos="fade-up">
                            <h1 class="sub_title">About MedHelp</h1>
                            <p class="card-text" style="padding-right: 10px; letter-spacing: 1px;"><span
                                    style="font-size: 30px; color: #00A3C8;">W</span>elcome to MedHelp, a leading
                                provider of innovative healthcare management solutions. Our mission is to revolutionize
                                healthcare administration and enhance patient care through cutting-edge technology and
                                exceptional service.At MedHelp, we are dedicated to revolutionizing the way healthcare
                                organizations manage their operations. Our Hospital Management System (HMS) is the
                                culmination of cutting-edge technology.</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <img src="https://thumbs.dreamstime.com/b/doctors-hospital-modern-medicine-group-successful-background-61422906.jpg"
                            class="img-fluid" alt="...">
                    </div>

                </div>
            </div>
        </div>
    </div>

    <!-- state counter -->
    <div class="state-container">
        <div class="container">
            <div class="state-section text-white">
                <div class="state-box text-center bg-info-subtle">
                    <h1 class="counter" data-target="211">0</h1>
                    <h5>Successful Operation</h5>
                    <p>By Our Experienced Surgeons</p>
                </div>
                <div class="state-box text-center bg-secondary-subtle">
                    <h1 class="counter" data-target="345">0</h1>
                    <h5>Special Care</h5>
                    <p>By The Expert Doctors</p>
                </div>
                <div class="state-box text-center bg-primary-subtle">
                    <h1 class="counter" data-target="412">0</h1>
                    <h5>Free Outdoor</h5>
                    <p>Checkups by Senior Doctors</p>
                </div>
                <div class="state-box text-center bg-dark-subtle">
                    <h1 class="counter" data-target="154">0</h1>
                    <h5>Blood & Urine</h5>
                    <p>Tests in Laboratory</p>
                </div>
            </div>
        </div>
    </div>

    <!-- new technology section -->
    <div class="py-5 mt-5">
        <div class="container">
            <div class="card mb-3" style="border: none;">
                <div class="row g-0">
                    <div id="carouselExampleControls" class="carousel slide col-md-6" data-bs-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img src="../img/aboutus-carousal-1.webp" class="d-block w-100" alt="...">
                            </div>
                            <div class="carousel-item">
                                <img src="https://www.medtecheurope.org/wp-content/themes/ak-medtecheurope-2018/assets/img/heading-2-02.jpg"
                                    class="d-block w-100" alt="...">
                            </div>
                            <div class="carousel-item">
                                <img src="../img/aboutus-carousal-3.jpg" class="d-block w-100" alt="...">
                            </div>
                        </div>
                        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls"
                            data-bs-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Previous</span>
                        </button>
                        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls"
                            data-bs-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Next</span>
                        </button>
                    </div>
                    <div class="col-md-6 px-2">
                        <div class="card-body">
                            <h1 class="sub_title" data-aos="fade-in">New Generation High Technology</h1>
                            <p class="card-text py-4" style="padding-right: 10px; letter-spacing: 1px;"><span
                                    style="font-size: 30px; color: #00A3C8;">W</span>e provide services in our hospital
                                by moving forward with confidence with constantly renewed technology and future-oriented
                                investments. It receives reports with state-of-the-art devices. We start the treatment
                                by making the correct diagnosis with our specialist physicians.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- patient reviews -->
    <div class="bg-body-tertiary py-5">
        <div class="container">
            <h5 class="main_title pb-4 text-center" data-aos="fade-out">TESTIMONIAL</h5>
            <h2 class="sub_title text-center" data-aos="fade-in">See What Are The Patients Saying</h2>
            <div class="patient-sec d-grid py-5">
                <div class="patient-info bg-white pt-2 px-2 rounded-5">
                    <div class="d-flex justify-content-around pt-2">
                        <img src="https://www.kevinashleyphotography.com/wp-content/uploads/2015/11/person(pp_w480_h610).jpg"
                            alt="" class="img-thumbnail border-primary p-1"
                            style="border-radius: 50%; width: 100px; height: 100px;">
                        <div class="align-self-center">
                            <h5>Rakesh Patel</h5>
                            <span style="font-size:150%;color:blue;">&starf;</span>
                            <span style="font-size:150%;color:blue;">&starf;</span>
                            <span style="font-size:150%;color:blue;">&starf;</span>
                            <span style="font-size:150%;color:blue;">&starf;</span>
                            <span style="font-size:150%;color:blue;">&star;</span>
                        </div>
                    </div>
                    <p class="p-3 patient-review">Competence and expertise of doctors and medical staff. Effectiveness
                        of communication between healthcare providers and patients. Friendliness and empathy of hospital
                        staff. Handling of insurance claims and assistance in financial matters.</p>
                </div>
                <div class="patient-info bg-white pt-2 px-2 rounded-5">
                    <div class="d-flex justify-content-around pt-2" style="">
                        <img src="https://tse1.mm.bing.net/th?id=OIP.GE6MMIFJI4DhvNk-7CngLQHaHe&pid=Api&P=0&h=220"
                            alt="" class="img-thumbnail border-primary p-1"
                            style="border-radius: 50%; width: 100px; height: 100px;">
                        <div class="align-self-center">
                            <h5>Suresh Raina</h5>
                            <span style="font-size:150%;color:blue;">&starf;</span>
                            <span style="font-size:150%;color:blue;">&starf;</span>
                            <span style="font-size:150%;color:blue;">&starf;</span>
                            <span style="font-size:150%;color:blue;">&starf;</span>
                            <span style="font-size:150%;color:blue;">&starf;</span>
                        </div>
                    </div>
                    <p class="p-3 patient-review">Accuracy of diagnosis and effectiveness of treatment. Ability of
                        doctors and nurses to explain medical conditions and treatment options. Overall appearance and
                        atmosphere of the hospital.</p>
                </div>
                <div class="patient-info bg-white pt-2 px-2 rounded-5">
                    <div class="d-flex justify-content-around pt-2" style="">
                        <img src="https://tse4.mm.bing.net/th?id=OIP.HQ5XGuO1-EXYotVNCdT8XwHaHN&pid=Api&P=0&h=220"
                            alt="" class="img-thumbnail border-primary p-1"
                            style="border-radius: 50%; width: 100px; height: 100px;">
                        <div class="align-self-center">
                            <h5>Ashvini Sharma</h5>
                            <span style="font-size:150%;color:blue;">&starf;</span>
                            <span style="font-size:150%;color:blue;">&starf;</span>
                            <span style="font-size:150%;color:blue;">&starf;</span>
                            <span style="font-size:150%;color:blue;">&star;</span>
                            <span style="font-size:150%;color:blue;">&star;</span>
                        </div>
                    </div>
                    <p class="p-3 patient-review">Clarity and transparency of billing and financial matters.
                        Accessibility and responsiveness of the emergency department. Quality of follow-up care and
                        support after discharg. Quality of care during emergency situations.</p>
                </div>
            </div>
        </div>
    </div>

    <!-- back to top -->
    <button type="button" title="Back to Top" class="text-center btn back-top" id="back-top">&#8593</button>

    <!-- footer -->
    <footer class="" style="background-color: #004861;">
        <div class="footer-container d-grid text-white-50 container py-4">
            <div class="py-3 d-flex flex-column">
                <h1>MedHelp</h1>
                <p class="pt-4" style="padding-right: 15%;">Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Distinctio aperiam ad quisquam voluptate aut ab?</p>
            </div>
            <div class="py-3 d-flex flex-column">
                <h4>Quick Links</h4>
                <a href="./index.php" class="text-white-50 py-1 w-50">Home</a>
                <!-- <a href="#" class="text-white-50 py-1 w-50">Dashboard</a> -->
                <a href="./Services.php" class="text-white-50 py-1 w-50">Services</a>
                <a href="#" class="text-white-50 py-1 w-50">About us</a>
                <a href="./Contact.php" class="text-white-50 py-1 w-50">Contact us</a>
            </div>
            <div class="py-3">
                <h4>Contact us</h4>
                <p><i class="bi-pin-fill"></i> 401-Shreeram complex, Ahmedabad - 392324</p>
                <p><i class="bi-telephone"></i> +91 9876543210</p>
                <p><i class="bi-envelope"></i> medhelp@gmail.com</p>
            </div>
            <div class="py-3">
                <a href="#" class="fa fa-instagram mx-2 bg-gradient p-2 text-white"></a>
                <a href="#" class="fa fa-facebook-official mx-2 bg-gradient p-2 text-white"></a>
                <a href="#" class="fa fa-linkedin mx-2 bg-gradient p-2 text-white"></a>
                <p><sup>©</sup>Do not sell or share My Professional information.</p>
            </div>
        </div>
    </footer>

    <!-- ViewModal -->
  <div class="modal fade mt-2" id="profileModal" tabindex="-1" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h1 class="modal-title fs-5" id="exampleModalLabel" style="color: #00A3C8;"><span
              style="font-size: 16px; color: #b2b2b2; letter-spacing: 1;" id="viewModelIdText">#Profile</span><br />
            <span id="viewModelNameText" style="margin-top: -10px">
              <?php echo $userName; ?>
            </span></h1>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <div style="display: flex; align-items: center; justify-content: space-around;">
            <div>
                <img src="<?php echo $userImg; ?>" alt="" style="width: 100%; height: 150px;">
            </div>
            <div>
                <div class="p-2">
                    <p><span style="color: #004861; font-weight: 800;">Name: </span> <span
                        id="viewModelEmailText"><?php echo $userName; ?></span></p>
                    <p><span style="color: #004861; font-weight: 800;">Email: </span> <span
                            id="viewModelEmailText"><?php echo $userEmail; ?></span></p>
                    <p><span style="color: #004861; font-weight: 800;">password: </span> <span
                            id="viewModelCreatedText"><?php echo $userpass; ?></span></p>
                </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn" style="padding: 8px 20px; background-color: #fff; color: red; border: 1px solid red; border-radius: 6px; margin-left: 48px;" onclick="logoutBtn()">Logout</button>
        </div>
      </div>
    </div>
  </div>

    <!-- script -->
    <script>
        AOS.init();
    </script>

    <script>
         const userName = "<?php echo $userName; ?>";
    const userType = "<?php echo $userType; ?>";

    console.log(userType)

    if (userName == "") {
      document.getElementById("login").hidden = false;
      document.getElementById("signin").hidden = false;
      document.getElementById("userDetaliContainer").hidden = true;

    }
    else {
      document.getElementById("userName").innerText = userName;
      document.getElementById("login").hidden = true;
      document.getElementById("signin").hidden = true;
    }


    if (userType.trim() == "admin") {
      document.getElementById("deshborad").hidden = false;
      document.getElementById("appoinment").hidden = true;
      document.getElementById("PatientsPage").hidden = true;
    }
    else if (userType.trim() == "receptionist") {
      document.getElementById("deshborad").hidden = true;
      document.getElementById("appoinment").hidden = false;
      document.getElementById("PatientsPage").hidden = false;
    }
    else if (userType.trim() == "doctor") {
      document.getElementById("deshborad").hidden = true;
      document.getElementById("appoinment").hidden = true;
      document.getElementById("PatientsPage").hidden = false;
    }
    else {
      document.getElementById("deshborad").hidden = true;
      document.getElementById("appoinment").hidden = false;
      document.getElementById("PatientsPage").hidden = true;
    }

    function appoinmentLink() {
        if (userName == "") {
        location.href = "./LoginPage.php";
      }
      else {
        location.href = "./AppointmentPage.php";
      }
    }

        let myButton = document.getElementById("back-top");
        window.onscroll = function () {
            scrollFunction();
        };
        function scrollFunction() {
            if (
                document.body.scrollTop > 200 ||
                document.documentElement.scrollTop > 400
            ) {
                myButton.style.display = "block";
            } else {
                myButton.style.display = "none";
            }
        }
        myButton.addEventListener("click", backToTop);
        function backToTop() {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
        }

        // counter javascript
        const counts = document.querySelectorAll('.counter');
        const speed = 150;
        counts.forEach((counter) => {
            function upData() {
                const target = Number(counter.getAttribute('data-target'));
                const count = Number(counter.innerText)
                const inc = target / speed
                if (count < target) {
                    counter.innerText = Math.floor(inc + count)
                    setTimeout(upData, 1)
                } else {
                    counter.innerText = target
                }
            }
            upData()
        })
        function logoutBtn(){
        $.ajax({
            url: '../componants/logout.php',
            method: 'post',
            success: function (result) {
                if(result == "done"){
                    $('#profileModal').modal('toggle');
                    location.replace("./LoginPage.php")
                }
            }
        })
    }
    </script>
</body>

</html>