-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 16, 2023 at 03:07 PM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hospital_management`
--

-- --------------------------------------------------------

--
-- Table structure for table `tblDoctors`
--

CREATE TABLE `tblDoctors` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `email` varchar(80) NOT NULL,
  `specialization` varchar(20) NOT NULL,
  `type` varchar(20) NOT NULL,
  `created` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tblDoctors`
--

INSERT INTO `tblDoctors` (`id`, `name`, `email`, `specialization`, `type`, `created`) VALUES
(6, 'Milan Kalathiya', 'milan@gmail.com', 'M.D', 'Visit', '16/09/23 06:14:14 pm'),
(7, 'Kenil Bhikadiya', 'kenil@gmail.com', 'M.B.B.H', 'Full Time', '16/09/23 06:14:40 pm'),
(8, 'Harsh Desai', 'harsh@gmail.com', 'M.D', 'Full Time', '16/09/23 06:15:03 pm'),
(9, 'Darshan Vala', 'darshan@gmail.com', 'M.H.B.S', 'Visit', '16/09/23 06:15:39 pm'),
(10, 'Ragnesh Golakiya', 'ragnesh@gmail.com', 'M.B.B.H', 'Full Time', '16/09/23 06:16:14 pm');

-- --------------------------------------------------------

--
-- Table structure for table `tblPatients`
--

CREATE TABLE `tblPatients` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phno` text NOT NULL,
  `doctor` varchar(100) NOT NULL,
  `disease` text NOT NULL,
  `date` varchar(100) NOT NULL,
  `time` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tblPatients`
--

INSERT INTO `tblPatients` (`id`, `name`, `email`, `phno`, `doctor`, `disease`, `date`, `time`) VALUES
(1, 'Ridham  Golakiya', 'ridham@gmail.com', '1234567890', 'Darshan Vala', 'Fever', '2023-09-16', '10:10 pm'),
(2, 'Rushit Dhola', 'rushit@gmail.com', '1234567890', 'Kenil Bhikadiya', 'Diabetes', '2023-09-20', '09:30 am'),
(3, 'Dhruv Bhikadiya ', 'dhruv@gmail.com', '1234567890', 'Ragnesh Golakiya', 'Hadek', '2023-09-16', '07:30 pm'),
(4, 'Rushit Dhola', 'rushit@gmail.com', '1234567890', 'Kenil Bhikadiya', 'Fever', '2023-09-16', '06:32 pm'),
(5, 'Gautam Patel', 'gautam@gmail.com', '1234567890', 'Milan Kalathiya', 'Fever', '2023-09-16', '06:34 pm');

-- --------------------------------------------------------

--
-- Table structure for table `tblReceptionist`
--

CREATE TABLE `tblReceptionist` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `created` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tblReceptionist`
--

INSERT INTO `tblReceptionist` (`id`, `name`, `email`, `created`) VALUES
(2, 'Mahesh Sharma', 'mahesh@gmail.com', '16/09/23 06:12:35 pm'),
(3, 'Suresh Singh', 'suresh@gmail.com', '16/09/23 06:13:04 pm'),
(4, 'Anand Patel', 'anad@gmail.com', '16/09/23 06:13:25 pm'),
(5, 'Jay Goyani', 'jay@gmail.com', '16/09/23 06:13:46 pm');

-- --------------------------------------------------------

--
-- Table structure for table `tblRegister`
--

CREATE TABLE `tblRegister` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `email` text NOT NULL,
  `password` text NOT NULL,
  `type` text NOT NULL,
  `picture` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tblRegister`
--

INSERT INTO `tblRegister` (`id`, `name`, `email`, `password`, `type`, `picture`) VALUES
(1, 'admin', 'admin@gmail.com', 'admin123', 'admin', '../img/user.jpeg'),
(2, 'Mahesh Sharma', 'mahesh@gmail.com', '12345', 'receptionist', '../img/user.jpeg'),
(3, 'Suresh Singh', 'suresh@gmail.com', '12345', 'receptionist', '../img/user.jpeg'),
(4, 'Anand Patel', 'anad@gmail.com', '12345', 'receptionist', '../img/user.jpeg'),
(5, 'Jay Goyani', 'jay@gmail.com', '12345', 'receptionist', '../img/user.jpeg'),
(6, 'Milan Kalathiya', 'milan@gmail.com', '12345', 'doctor', '../img/user.jpeg'),
(7, 'Kenil Bhikadiya', 'kenil@gmail.com', '12345', 'doctor', '../img/user.jpeg'),
(8, 'Harsh Desai', 'harsh@gmail.com', '123', 'doctor', '../img/user.jpeg'),
(9, 'Darshan Vala', 'darshan@gmail.com', '12345', 'doctor', '../img/user.jpeg'),
(10, 'Ragnesh Golakiya', 'ragnesh@gmail.com', '12345', 'doctor', '../img/user.jpeg'),
(11, 'Dhruv Bhikadiya', 'dhruv@gmail.com', '12345', 'user', '../img/user.jpeg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tblPatients`
--
ALTER TABLE `tblPatients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblRegister`
--
ALTER TABLE `tblRegister`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tblPatients`
--
ALTER TABLE `tblPatients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tblRegister`
--
ALTER TABLE `tblRegister`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
