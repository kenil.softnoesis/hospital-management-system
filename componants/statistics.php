<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Deshborad</title>

    <link rel="stylesheet" href="../css/AdminDeshborad.css">
  
    <!-- bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet"
      integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm"
      crossorigin="anonymous"></script>
  
    <!-- Chart -->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  
    <!-- icons -->
    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.js"></script>
  
</head>
<body >
  
<div class="ps-5 mt-5">
    <p style="color: #004861; font-size: 26px; font-weight: 600;">Welcome Admin,</p>
    <p class="ms-1" style="color: #b2b2b2; font-size: 14px; margin-top: -15px;">Let's cheack the update for today
      here...</p>

    <div class="mt-5 row " style="width: 98%;">
      <div class="col-md-3">
        <div class="p-3 box" style="height: auto; background-color: #fff">
          <h6 style="font-weight: 600;">Total Patients</h6>
          <p style="background-color: #00A3C8; height:2px; width: 22%; margin-top: -5px;"></p>
          <div style="display: flex; flex-direction: row; align-items: center;">
            <ion-icon  name="person-circle-outline" style="font-size: 50px; color: #00A3C8;"></ion-icon>
            <h3 style="color: #000; margin-left: 10px; margin-top: 10px;">50</h3>
          </div>

        </div>
      </div>

      <div class="col-md-3">
        <div class="p-3 box" style="height: auto; background-color: #fff">
          <h6 style="font-weight: 600;">Doctors Availables</h6>
          <p style="background-color: #00A3C8; height:2px; width: 30%; margin-top: -5px;"></p>
          <div style="display: flex; flex-direction: row; align-items: center;">
            <ion-icon name="accessibility-outline" style="font-size: 40px; color: #FFA500;"></ion-icon>
            <h3 style="color: #000; margin-left: 10px; margin-top: 10px;">50</h3>
          </div>
        </div>
      </div>


      <div class="col-md-3">
        <div class="p-3 box" style="height: auto; background-color: #fff;">
          <h6 style="font-weight: 600;">Bed Availables</h6>
          <p style="background-color: #00A3C8; height:2px; width: 26%; margin-top: -5px;"></p>
          <div style="display: flex; flex-direction: row; align-items: center;">
            <ion-icon name="bed-outline"style="font-size: 40px; color: #008000;"></ion-icon>
            <h3 style="color: #000; margin-left: 10px; margin-top: 10px;">50</h3>
          </div>
        </div>
      </div>

      <div class="col-md-3">
        <div class="p-3 box" style="height: auto; background-color: #B8E2F2;">
          <h6 style="font-weight: 600; color: #000;">Token Number</h6>
          <p style="background-color: #00A3C8; height:2px; width: 33%; margin-top: -5px;"></p>
          
          <div style="display: flex; flex-direction: row; align-items: center;">
            <ion-icon name="ticket-outline"style="font-size: 40px; color: #004861;"></ion-icon>
            <h3 style="color: #000; margin-left: 10px; margin-top: 10px;">50</h3>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row ps-5 mt-5" style="width: 98%; align-items: center; margin-bottom: 20px;">
    <div class="col-md-5">
      <div class="p-3 box" style="height: 430px; background-color: #fff;">
        <h6 style="font-weight: 600; color: #000;">Department</h6>

        <div id="myChart" style="width:100%; height:380px; align-items: center;">
        </div>

      </div>
    </div>

    <div class="col-md-7">
      <div class="p-3 box" style="height: auto; background-color: #fff;">
        <h6 style="font-weight: 600; color: #000;">Lab reports to be sent (5)</h6>

        <div class="row box mt-4"
          style="align-items: center; padding-left: 5px; padding-right: 5px; padding-top: 5px; padding-bottom: 5px; width: 98%; margin-left: 1%;">
          <div class="col-md-3">
            RI124
          </div>
          <div class="col-md-3">
            Ram Mehata
          </div>
          <div class="col-md-3">
            X.ray.pdf
          </div>
          <div class="col-md-3 pt-1 pb-1">
            <button
              style="border: 0; padding: 8px 15px; color: #fff; background-color: green; border-radius: 6px;">Share</button>
          </div>
        </div>

        <div class="row box mt-3"
          style="align-items: center; padding-left: 5px; padding-right: 5px; padding-top: 5px; padding-bottom: 5px; width: 98%; margin-left: 1%;">
          <div class="col-md-3">
            RI124
          </div>
          <div class="col-md-3">
            Ram Mehata
          </div>
          <div class="col-md-3">
            X.ray.pdf
          </div>
          <div class="col-md-3 pt-1 pb-1">
            <button
              style="border: 0; padding: 8px 15px; color: #fff; background-color: green; border-radius: 6px;">Share</button>
          </div>
        </div>

        <div class="row box mt-3"
          style="align-items: center; padding-left: 5px; padding-right: 5px; padding-top: 5px; padding-bottom: 5px; width: 98%; margin-left: 1%;">
          <div class="col-md-3">
            RI124
          </div>
          <div class="col-md-3">
            Ram Mehata
          </div>
          <div class="col-md-3">
            X.ray.pdf
          </div>
          <div class="col-md-3 pt-1 pb-1">
            <button
              style="border: 0; padding: 8px 15px; color: #fff; background-color: green; border-radius: 6px;">Share</button>
          </div>
        </div>

        <div class="row box mt-3"
          style="align-items: center; padding-left: 5px; padding-right: 5px; padding-top: 5px; padding-bottom: 5px; width: 98%; margin-left: 1%;">
          <div class="col-md-3">
            RI124
          </div>
          <div class="col-md-3">
            Ram Mehata
          </div>
          <div class="col-md-3">
            X.ray.pdf
          </div>
          <div class="col-md-3 pt-1 pb-1">
            <button
              style="border: 0; padding: 8px 15px; color: #fff; background-color: green; border-radius: 6px;">Share</button>
          </div>
        </div>
        <div class="row box mt-3"
          style="align-items: center; padding-left: 5px; padding-right: 5px; padding-top: 5px; padding-bottom: 5px; width: 98%; margin-left: 1%;">
          <div class="col-md-3">
            RI124
          </div>
          <div class="col-md-3">
            Ram Mehata
          </div>
          <div class="col-md-3">
            X.ray.pdf
          </div>
          <div class="col-md-3 pt-1 pb-1">
            <button
              style="border: 0; padding: 8px 15px; color: #fff; background-color: green; border-radius: 6px;">Share</button>
          </div>
        </div>
      </div>
    </div>
  </div>

  <script>
    google.charts.load('current', { 'packages': ['corechart'] });
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
      const data = google.visualization.arrayToDataTable([
        ['dept', 'Mhl'],
        ['General', 54.8],
        ['Gyno', 48.6],
        ['ENT', 44.4],
        ['Scans', 30.5]
      ]);

      const options = {
        is3D: true,
      };

      const chart = new google.visualization.PieChart(document.getElementById('myChart'));
      chart.draw(data, options);
    }
  </script>
</body>
</html>