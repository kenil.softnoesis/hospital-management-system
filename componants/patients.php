<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Petients</title>

    <!-- bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm"
        crossorigin="anonymous"></script>

     <!-- jquery-->
     <script src="../js/jquery.js"></script>

</head>

<style>
    tr {
        text-align: center;
    }
</style>

<body>
    <div class="ps-5 mt-5">

        <div style="display: flex; flex-direction: row; align-items: center; justify-content: space-between; margin-right: 5%;">
            <p style="color: #004861; font-size: 26px; font-weight: 600;">All Patients</p>
        </div>

        <div class="mt-4" style="width: 90%; margin-left: 5%;">
            <input class="form-control me-2 p-2" style=" box-shadow: 0px 0px 5px -3px #000;" type="search" id="search" placeholder="Search Patients" aria-label="Search">
        </div>

        <div class="mt-4">
            <table class="table" style="width: 95%;">
                <thead>
                    <tr>
                        <th style="color: #00A3C8;">PatientId</th>
                        <th style="color: #00A3C8;">Name</th>
                        <th style="color: #00A3C8;">Email</th>
                        <th style="color: #00A3C8;">Doctor</th>
                        <th style="color: #00A3C8;">Disease</th>
                        <th style="color: #00A3C8;">Date</th>
                        <th style="color: #00A3C8;">Time</th>
                        
                    </tr>
                </thead>
                <?php
                        include "connection.php";
                    ?>
                <tbody id="tableData">
                
                </tbody>
               
            </table>

        </div>
    </div>
    
    <script>

        $("#search").keyup(function(){
            let search = $("#search").val();
            pageLoad(search)
        })

        pageLoad();
        function pageLoad(search) {
            $.ajax({
                url: 'select.php',
                method: 'post',
                data: { 'table': "patientsSimple", "search" : search},
                success: function (result) {
                    $("#tableData").html(result)
                }
            })
        }
        // document.getElementById('datePicker').valueAsDate = new Date();

        function SubmitDate(){
            document.getElementById('datePickerForm').submit()
        }
</script>
</body>
</html>