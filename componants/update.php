<?php
    
    include 'connection.php';


    if($_POST['table'] == "receptionsit"){
        $id = $_POST['editId'];
        $name = $_POST['editName'];
        $email = $_POST['editEmail'];

        if($name != "" && filter_var($email, FILTER_VALIDATE_EMAIL)){
            $sql = "UPDATE `tblRegister` SET `name`='$name',`email`='$email' WHERE `id`=$id";
            $result = mysqli_query($conn, $sql) or die("Unscuccessfull");
            $sql = "UPDATE `tblReceptionist` SET `name`='$name',`email`='$email' WHERE `id`=$id";
            $result = mysqli_query($conn, $sql) or die("Unscuccessfull");
            
            if($result){
                echo "updated";
            }
        }
        else{
            echo "Not updated";
        }
    }
    else if($_POST['table'] == "doctor"){
        $id = $_POST['editId'];
        $name = $_POST['editName'];
        $email = $_POST['editEmail'];
        $sep = $_POST['editSec'];
        $type = $_POST['editType'];

        if($name != "" && filter_var($email, FILTER_VALIDATE_EMAIL) && $id != "" && $sep != "" && $type != ""){
            $sql = "UPDATE `tblRegister` SET `name`='$name',`email`='$email' WHERE `id`=$id";
            $result = mysqli_query($conn, $sql) or die("Unscuccessfull");
            $sql = "UPDATE `tblDoctors` SET `name`='$name',`email`='$email',`specialization`='$sep',`type`='$type' WHERE `id`=$id";
            $result = mysqli_query($conn, $sql) or die("Unscuccessfull");

            if($result){
               echo "Updated";
            }
        }
        else{
            echo "Not updated";
        }
    }
    else if($_POST['table'] == "patients"){
        $id = $_POST['editId'];
        $name = $_POST['editName'];
        $email = $_POST['editEmail'];
        $doctor = $_POST['editDoctor'];
        $Disease = $_POST['editDisease'];
        $date = $_POST['editDatePicker'];
        $time = $_POST['editTimePicker'];
        $t = strtotime($time);
        $custom_time = date("h:i a", $t);

        if($name != "" && filter_var($email, FILTER_VALIDATE_EMAIL) && $id != "" && $Disease!= ""){
            $sql = "UPDATE `tblPatients` SET `name`='$name',`email`='$email',`doctor`='$doctor',`disease`='$Disease',`date`='$date',`time`='$custom_time' WHERE `id`=$id";
            $result = mysqli_query($conn, $sql) or die("Unscuccessfull");

            if($result){
                echo "Updated";
            }
        }
        else{
            echo "Not updated";
        }
    }
?>