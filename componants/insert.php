<?php
   
   include 'connection.php';
   date_default_timezone_set("Asia/Calcutta");
   $currDate = date("d/m/y h:i:s a");

   if($_POST['table'] == "receptionsit"){
      $name = $_POST['addName'];
      $email = $_POST['addEmail'];
      $pass = $_POST ['addPass'];
      $img = $_POST['userImg'];

      if($name != "" && filter_var($email, FILTER_VALIDATE_EMAIL) && $pass != ""  && $img != ""){
         $sql = "INSERT INTO `tblRegister`(`name`, `email`, `password`, `type`, `picture`) VALUES ('$name', '$email', '$pass', 'receptionist', '$img')";
         $result = mysqli_query($conn, $sql);
         $sql = "SELECT * FROM `tblRegister` ORDER BY `id` DESC LIMIT 1;";
         $result = mysqli_query($conn, $sql) or die("Unscuccessfull");
         if(mySqli_num_rows($result) == 1){
            while($row = mysqli_fetch_assoc($result)){
               $did = $row['id'];
            }
         }
         $sql = "INSERT INTO `tblReceptionist`(`id`, `name`, `email`, `created`) VALUES ($did, '$name','$email','$currDate')";
         $result = mysqli_query($conn, $sql) or die("Unscuccessfull");

         if($result){
            header("location: receptionist.php?label=done");
         }
      }
      else{
         echo "<br />Please  enter valid data";
      }
   }
   else if($_POST['table'] == "doctor"){
      $name = $_POST['addName'];
      $email = $_POST['addEmail'];
      $spe = $_POST['addSpecialization'];
      $type = $_POST['addType'];
      $pass = $_POST ['addPass'];
      $img = $_POST['userImg'];

      if($name != "" && filter_var($email, FILTER_VALIDATE_EMAIL) && $spe != "" && $type != "" && $pass != ""){
         $sql = "INSERT INTO `tblRegister`(`name`, `email`, `password`, `type`, `picture`) VALUES ('$name', '$email', '$pass', 'doctor', '$img')";
         $result = mysqli_query($conn, $sql);
         $sql = "SELECT * FROM `tblRegister` ORDER BY `id` DESC LIMIT 1;";
         $result = mysqli_query($conn, $sql) or die("Unscuccessfull");
         if(mySqli_num_rows($result) == 1){
            while($row = mysqli_fetch_assoc($result)){
               $did = $row['id'];
            }
         }
         $sql = "INSERT INTO `tblDoctors`(`id`, `name`, `email`, `specialization`, `type`, `created`) VALUES ($did, '$name','$email', '$spe', '$type', '$currDate')";
         $result = mysqli_query($conn, $sql) or die("Unscuccessfull");
         if($result ){
            header("location: doctors.php?label=done");
        }
      }
      else{
         echo "<br />Please  enter valid data";
      }
   }
   else if($_POST['table'] == "patients"){
      $fname = $_POST['fname'];
      $lname = $_POST['lname'];
      $email = $_POST['email'];
      $phno = $_POST['phno'];
      $doctor = $_POST['doctor'];
      $disease = $_POST['disease'];
      $datePicker = $_POST['datePicker'];
      $timePicker = $_POST['timePicker'];
      $name = $fname." ".$lname;
      $t = strtotime($timePicker);
      $custom_time = date("h:i a", $t);
   
     if($fname != "" && $lname != "" && filter_var($email, FILTER_VALIDATE_EMAIL) && $phno != "" && $doctor != "" && $disease !="" && $datePicker != "" && $timePicker != ""){
     
      $sql = "INSERT INTO `tblPatients`(`name`, `email`, `phno`, `doctor`, `disease`, `date`, `time`) VALUES ('$name', '$email', '$phno', '$doctor', '$disease', '$datePicker', '$custom_time')";    
      $result = mysqli_query($conn,$sql);
      if($result){
         header("location: ../html/PatientsPage.php");
      }
     }
     else{
      echo "<br />Please  enter valid data";
      }
   }
   else if($_POST['table'] == "register"){
      $name = $_POST["name"];
      $email = $_POST["email"];
      $pass = $_POST["pass"];
      $type = $_POST["type"];
      $img = $_POST["picture"];

      if($name != "" && filter_var($email, FILTER_VALIDATE_EMAIL) && $pass != "" && $type != "" && $img != ""){
         $sql = "INSERT INTO `tblRegister`(`name`, `email`, `password`, `type`, `picture`) VALUES ('$name', '$email', '$pass', 'user', '$img')";
         $result = mysqli_query($conn, $sql);

         if($result){
            echo "Inserted";
         }
         else{
            echo "Not inserted";
         }
      }
   }
?>