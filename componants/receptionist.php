<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Receptionist</title>

    <!-- bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm"
        crossorigin="anonymous"></script>

    <!-- icons -->
    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.js"></script>

    <!-- jquery-->
    <script src="../js/jquery.js"></script>

</head>

<style>
    tr {
        text-align: center;
        margin-top: 5px;
    }
</style>

<body>
    <div class="ps-5 mt-5">
        <div
            style="display: flex; flex-direction: row; align-items: center; justify-content: space-between; margin-right: 2%;">
            <p style="color: #004861; font-size: 26px; font-weight: 600;">All Receptionist</p>
            <button data-bs-toggle="modal" data-bs-target="#example3Modal"
                style="background-color: #fff; color: #00A3C8; border: 1px solid #00A3C8; padding: 8px 20px; border-radius:8px;  box-shadow: 0px 0px 5px -5px #000;">Add
                New</button>
        </div>

        <div class="mt-4" style="width: 90%; margin-left: 5%;">
            <input class="form-control me-2 p-2" style=" box-shadow: 0px 0px 5px -3px #000;" type="search" id="search" placeholder="Search Receptionists" aria-label="Search">
        </div>

        <div class="mt-4">
            <table class="table" style="width: 98%;">
                <thead>
                    <tr>
                        <th style="color: #00A3C8;">Name</th>
                        <th style="color: #00A3C8;">Email</th>
                        <th style="color: #00A3C8;">View</th>
                        <th style="color: #00A3C8;">Edit</th>
                        <th style="color: #00A3C8;">Delete</th>
                    </tr>
                </thead>
                <tbody class="mt-2" id="tableData">
                </tbody>
            </table>
        </div>
    </div>

    <!-- ViewModal -->
    <div class="modal fade mt-3" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="exampleModalLabel" style="color: #00A3C8;"><span
                            style="font-size: 16px; color: #b2b2b2; letter-spacing: 1;"
                            id="viewModelIdText">#1</span><br /> <span id="viewModelNameText"
                            style="margin-top: -10px">Tarakh Mehta</span></h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div style="display: flex; align-items: center; justify-content: space-between;">
                        <div>
                            <div class="p-2" style="height: 150px;">
                                <img src="../img/user.jpeg"
                                    class="img-fluid rounded-circle align-center" alt="profile"
                                    style="width: 100%; height: 100%;" id="viewModelImg">
                            </div>
                        </div>
                        <div>
                            <div class="p-2">
                                <p><span style="color: #004861; font-weight: 800;">Email: </span> <span
                                        id="viewModelEmailText"></span></p>
                                <p><span style="color: #004861; font-weight: 800;">Created Date: </span> <span
                                        id="viewModelCreatedText"></span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- EditModal -->
    <div class="modal fade mt-3" id="example2Modal" tabindex="-1" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="exampleModalLabel" style="color: #00A3C8;"><span
                            style="font-size: 16px; color: #b2b2b2; letter-spacing: 1;" id="editModelIdText"
                            name="editId">#1</span><br /><span id="editModelNameText">Tarakh Mehta</span></h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="p-1">
                        <div class="mb-1" style="display: flex; flex-direction: row; align-items: center;">
                            <label for="exampleFormControlInput1" class="form-label mt-2">Name:</label>
                            <input type="text" class="form-control ms-2" id="editModelInputName" name="editName">
                        </div>
                    </div>

                    <div class="p-1">
                        <div class="mb-1" style="display: flex; flex-direction: row; align-items: center;">
                            <label for="exampleFormControlInput1" class="form-label mt-2">Email:</label>
                            <input type="email" class="form-control ms-2" id="editModelInputEmail" name="editEmail">
                        </div>
                    </div>

                    <input type="number" class="form-control ms-2" id="editModelIdInput" name="editId" hidden>

                </div>
                <div id='showErr' class="alert alert-danger ms-3 me-3" style="display: none;" role="alert">
                    Please enter valid value for all the fields for update receptionist
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="button" class="btn" id="editBtn" style="background-color: #00A3C8; color: #fff;"
                        onclick="EditRecepfun()">Save
                        changes</button>
                </div>
            </div>
        </div>
    </div>

    <!-- AddModal -->
    <div class="modal fade mt-3" id="example3Modal" tabindex="-1" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <form id="addForm" action="insert.php?table=receptionsit" method="post" class="modal-content">
                <div class="modal-header">
                    <h4 style="color:  #00A3C8;">Add New Receptionist</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="p-1">
                        <div class="mb-1" style="display: flex; flex-direction: row; align-items: center;">
                            <label for="exampleFormControlInput1" class="form-label mt-2">Name:</label>
                            <input type="text" class="form-control ms-2" id="addModelInputName" name="addName">
                        </div>
                    </div>

                    <div class="p-1">
                        <div class="mb-1" style="display: flex; flex-direction: row; align-items: center;">
                            <label for="exampleFormControlInput1" class="form-label mt-2">Email:</label>
                            <input type="email" class="form-control ms-2" id="addModelInputEmail" name="addEmail">
                        </div>
                    </div>

                    <div class="p-1">
                        <div class="mb-1" style="display: flex; flex-direction: row; align-items: center;">
                            <label for="exampleFormControlInput1" class="form-label mt-2">Password:</label>
                            <input type="password" class="form-control ms-2" id="addModelInputPass" name="addPass">
                        </div>
                    </div>

                    <input type="text" value="../img/user.jpeg" name="userImg" hidden>
                </div>
                <div id='addshowErr' class="alert alert-danger ms-3 me-3" style="display: none;" role="alert">
                    Please enter valid value for all the fields for add receptionist
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="button" class="btn" style="background-color: #00A3C8; color: #fff;" name="addBtn"
                        id="addBtn" onclick="addRecepFun()">Add Receptionist</button>
                </div>
            </form>
        </div>
    </div>

    <script>
     
        $("#search").keyup(function(){
            let search = $("#search").val();
            pageLoad(search)
        })

        pageLoad();
        function pageLoad(search) {
            $.ajax({
                url: 'select.php',
                method: 'post',
                data: { 'table': "receptionsit", "search" : search},
                success: function (result) {
                    $("#tableData").html(result)
                }
            })
        }

        function ViewDetailBtn(e) {
            document.getElementById('viewModelIdText').innerText = '#' + e.parentNode.firstElementChild.textContent.trim();
            document.getElementById('viewModelNameText').innerText = e.parentNode.firstElementChild.nextElementSibling.textContent.trim();
            document.getElementById('viewModelEmailText').innerText = e.parentNode.firstElementChild.nextElementSibling.nextElementSibling.textContent.trim();
            document.getElementById('viewModelCreatedText').innerText =e.parentNode.firstElementChild.nextElementSibling.nextElementSibling.nextElementSibling.textContent.trim()
            // document.getElementById("viewModelImg").src = e.parentNode.firstElementChild.nextElementSibling.nextElementSibling.nextElementSibling.textContent.trim();;
        }

        function editViewBtn(e) {
            document.getElementById('editModelIdText').innerText = '#' + e.parentNode.firstElementChild.textContent.trim();
            document.getElementById('editModelIdInput').value = e.parentNode.firstElementChild.textContent.trim();
            document.getElementById('editModelNameText').innerText = e.parentNode.firstElementChild.nextElementSibling.textContent.trim();
            document.getElementById('editModelInputName').value = e.parentNode.firstElementChild.nextElementSibling.textContent.trim();
            document.getElementById('editModelInputEmail').value = e.parentNode.firstElementChild.nextElementSibling.nextElementSibling.textContent.trim();
        }

        function EditRecepfun() {
            let name = document.getElementById('editModelInputName').value;
            let email = document.getElementById('editModelInputEmail').value;
            let editId = document.getElementById('editModelIdInput').value;
            let name_pattern = /[a-z A-Z]/i;
            let email_pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

            if (name.match(name_pattern) && email.match(email_pattern)) {
                document.getElementById('showErr').style.display = "none";
                $.ajax({
                    url: 'update.php',
                    method: 'post',
                    data: { 'table': "receptionsit", "editId": editId, "editName": name, "editEmail": email },
                    success: function (result) {
                        pageLoad();
                        $('#example2Modal').modal('toggle');
                    }
                })
            }
            else {
                document.getElementById('showErr').style.display = "block";
            }
        }

        function addRecepFun() {
            let name = document.getElementById('addModelInputName').value;
            let email = document.getElementById('addModelInputEmail').value;
            let pass = document.getElementById('addModelInputPass').value;
            let name_pattern = /[a-z A-Z]/i;
            let email_pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

            if (name.match(name_pattern) && email.match(email_pattern) && pass != "") {
                console.log("valid")
                document.getElementById('addshowErr').style.display = "none";
                $.ajax({
                    url: 'insert.php',
                    method: 'post',
                    data: { 'table': "receptionsit", "addName": name, "addEmail": email, "addPass": pass, "userImg": "../img/user.jpeg" },
                    success: function (result) {
                        pageLoad();
                        document.getElementById('addModelInputName').value = "";
                        document.getElementById('addModelInputEmail').value = "";
                        document.getElementById('addModelInputPass').value = "";
                        $('#example3Modal').modal('toggle');
                        
                    }
                })
            }
            else {
                console.log("invalid")
                document.getElementById('addshowErr').style.display = "block";
            }
        }

        function deleteBtn(e) {
            let id = e.parentNode.firstElementChild.textContent.trim();
            $.ajax({
                url: 'delete.php',
                method: 'post',
                data: { 'table': "receptionsit", "id": id },
                success: function (result) {
                    pageLoad();
                }
            })
        }
    </script>

</body>

</html>